## Services package for Laravel 6+

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

The service provider should automatically register itself under Laravel.
If not, you can manually add `Smorken\Service\ServiceProvider::class` to the
providers section of `config/app.php`.
