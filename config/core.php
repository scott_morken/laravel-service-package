<?php

return [
    \Smorken\Service\Contracts\Services\FilterService::class => \Smorken\Service\Services\FilterService::class,
    \Smorken\Service\Contracts\Services\GateService::class => \Smorken\Service\Services\GateService::class,
    \Smorken\Service\Contracts\Services\GuardService::class => \Smorken\Service\Services\GuardService::class,
    \Smorken\Service\Contracts\Services\RequestService::class => \Smorken\Service\Services\RequestService::class,
    \Smorken\Service\Contracts\Services\ValidatorService::class => \Smorken\Service\Services\ValidatorService::class,
];
