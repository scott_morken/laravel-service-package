<?php

return [
    'autoload' => env('SERVICES_AUTOLOAD', false),
    'path' => env('SERVICES_INVOKABLE_PATH', 'Providers/Services'),
    'invokables' => [],
];
