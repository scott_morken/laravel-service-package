<?php

namespace Smorken\Service\Contracts\Enums;

interface GateBaseNames
{
    public const CREATE = 'create';

    public const DELETE = 'delete';

    public const DESTROY = 'destroy';

    public const FORCE_DELETE = 'forceDelete';

    public const INDEX = 'index';

    public const RESTORE = 'restore';

    public const UPDATE = 'update';

    public const VIEW = 'view';

    public const VIEW_ANY = 'viewAny';
}
