<?php

namespace Smorken\Service\Contracts\Enums;

interface RetrieveTypes
{
    public const DELETE = 'delete';

    public const UPDATE = 'update';

    public const VIEW = 'view';
}
