<?php

namespace Smorken\Service\Contracts\Enums;

interface SaveTypes
{
    public const CREATE = 'create';

    public const UPDATE = 'update';
}
