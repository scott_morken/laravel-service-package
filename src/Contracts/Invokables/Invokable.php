<?php

namespace Smorken\Service\Contracts\Invokables;

use Illuminate\Contracts\Foundation\Application;

interface Invokable
{
    public function __invoke(): void;

    public function getApp(): Application;
}
