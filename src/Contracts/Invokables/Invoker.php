<?php

namespace Smorken\Service\Contracts\Invokables;

use Illuminate\Contracts\Foundation\Application;

interface Invoker
{
    public function getBasePath(): string;

    public function getConfigItem(string $key, mixed $default = null): mixed;

    public function handle(Application $app): void;
}
