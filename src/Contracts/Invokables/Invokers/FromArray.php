<?php

namespace Smorken\Service\Contracts\Invokables\Invokers;

interface FromArray extends Handler
{
    public function getClasses(): array;
}
