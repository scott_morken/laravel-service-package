<?php

namespace Smorken\Service\Contracts\Invokables\Invokers;

interface FromFilesystem extends Handler
{
    public function getPath(): string;
}
