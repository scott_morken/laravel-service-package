<?php

namespace Smorken\Service\Contracts\Invokables\Invokers;

use Illuminate\Contracts\Foundation\Application;

interface Handler
{
    public function handle(Application $app): array;
}
