<?php

namespace Smorken\Service\Contracts\Invokables;

interface TokenParser
{
    public static function getClass(string $file): ?string;

    public static function getClasses(string $file): array;
}
