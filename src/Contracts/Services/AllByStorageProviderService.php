<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\IndexResult;

interface AllByStorageProviderService extends IndexService, StorageProviderService
{
    public function getByRequest(Request $request): IndexResult;
}
