<?php

namespace Smorken\Service\Contracts\Services;

use Smorken\Service\Contracts\Services\VO\VOResult;

interface BaseService extends HasServices
{
    public function implements(string $interface): bool;

    public function newVO(array $params): VOResult;

    public function setVOClass(string $voClass): void;
}
