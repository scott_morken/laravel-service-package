<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Http\Request;
use Smorken\Model\Contracts\Model;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Contracts\Services\VO\VOResult;

interface CreateService extends BaseService, HasRequestService, HasValidatorService
{
    public function createFromAttributes(array $attributes): VOResult|ModelResult;

    public function createFromRequest(Request $request): VOResult|ModelResult;

    public function getAttributesFromRequest(Request $request): array;

    public function getModel(): ?Model;
}
