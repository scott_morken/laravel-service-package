<?php

namespace Smorken\Service\Contracts\Services;

interface CreateServiceByStorageProviderService extends CreateService, StorageProviderService {}
