<?php

namespace Smorken\Service\Contracts\Services;

use Smorken\Storage\Contracts\Base;

interface CrudByStorageProviderServices extends CrudServices
{
    public static function createByStorageProvider(
        Base $provider,
        array $additionalServices = []
    ): static;
}
