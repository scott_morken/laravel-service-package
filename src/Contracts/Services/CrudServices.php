<?php

namespace Smorken\Service\Contracts\Services;

interface CrudServices extends Factory
{
    public function getCreateService(): CreateService;

    public function getDeleteService(): DeleteService;

    public function getRetrieveService(): RetrieveService;

    public function getUpdateService(): UpdateService;

    public function setCreateService(CreateService $providerService): void;

    public function setDeleteService(DeleteService $providerService): void;

    public function setRetrieveService(RetrieveService $providerService): void;

    public function setUpdateService(UpdateService $providerService): void;
}
