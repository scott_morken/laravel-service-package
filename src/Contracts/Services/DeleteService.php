<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Contracts\Services\VO\VOResult;

interface DeleteService extends BaseService
{
    public function deleteFromId(int|string $id): VOResult|ModelResult;

    public function deleteFromRequest(Request $request, int|string $id): VOResult|ModelResult;
}
