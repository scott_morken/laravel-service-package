<?php

namespace Smorken\Service\Contracts\Services;

use Smorken\Model\Contracts\Model;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Contracts\Services\VO\VOResult;

interface DeleteServiceByStorageProviderService extends DeleteService, StorageProviderService
{
    public function canDelete(Model $model): bool;

    public function deleteFromModel(Model $model): VOResult|ModelResult;
}
