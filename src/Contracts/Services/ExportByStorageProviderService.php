<?php

namespace Smorken\Service\Contracts\Services;

interface ExportByStorageProviderService extends ExportService, StorageProviderService {}
