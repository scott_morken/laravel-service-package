<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Http\Request;
use Smorken\Export\Contracts\Export;
use Smorken\Service\Contracts\Services\VO\ExportResult;

interface ExportService extends BaseService, HasFilterService
{
    public function export(Request $request): ExportResult;

    public function getExporter(): Export;

    public function setExporter(Export $exporter): void;
}
