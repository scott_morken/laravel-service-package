<?php

namespace Smorken\Service\Contracts\Services;

interface Factory
{
    public function getService(string $name): BaseService;

    public function getServices(): array;

    public function setService(string $name, BaseService $service): void;

    public function setServices(array $services): void;
}
