<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

interface FilterService extends BaseService, HasRequestService, HasValidatorService
{
    public function getFilterFromRequest(Request $request): Filter;
}
