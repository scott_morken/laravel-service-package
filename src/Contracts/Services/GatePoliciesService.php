<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable;

interface GatePoliciesService
{
    public function after(Authenticatable $user, $ability): ?bool;

    public function before(Authenticatable $user, $ability): ?bool;

    public function create(Authenticatable $user, ?array $attributes = null): bool|Response;

    public function delete(Authenticatable $user, mixed $model): bool|Response;

    public function destroy(Authenticatable $user, mixed $model): bool|Response;

    public function forceDelete(Authenticatable $user, mixed $model): bool|Response;

    public function getAbilityToMethodMap(): array;

    public function getGate(): Gate;

    public function getModelClass(): string;

    public function index(Authenticatable $user): bool|Response;

    public function restore(Authenticatable $user, mixed $model): bool|Response;

    public function setGate(Gate $gate): void;

    public function update(Authenticatable $user, mixed $model): bool|Response;

    public function view(Authenticatable $user, mixed $model): bool|Response;

    public function viewAny(Authenticatable $user): bool|Response;
}
