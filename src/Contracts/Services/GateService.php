<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;

interface GateService extends BaseService
{
    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function check(string $gateName, mixed $args = null, bool $throwException = true): bool;

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function checkRequest(
        Request $request,
        string $gateName,
        mixed $args = null,
        bool $throwException = true
    ): bool;

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function checkUser(
        Authenticatable $user,
        string $gateName,
        mixed $args = null,
        bool $throwsException = true
    ): bool;

    public function getGate(): Gate;

    public function has(string $gateName): bool;

    public function setGate(Gate $gate): void;
}
