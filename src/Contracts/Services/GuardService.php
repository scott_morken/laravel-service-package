<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Contracts\Auth\Guard;

interface GuardService extends BaseService
{
    public function getGuard(): Guard;

    public function setGuard(Guard $guard): void;
}
