<?php

namespace Smorken\Service\Contracts\Services;

interface HasFilterService
{
    public function getFilterService(): ?FilterService;

    public function setFilterService(FilterService $filterService): void;
}
