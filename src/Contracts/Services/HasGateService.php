<?php

namespace Smorken\Service\Contracts\Services;

interface HasGateService
{
    public function getGateService(): ?GateService;

    public function setGateService(GateService $gateService): void;
}
