<?php

namespace Smorken\Service\Contracts\Services;

interface HasGuardService
{
    public function getGuardService(): ?GuardService;

    public function setGuardService(GuardService $guardService): void;
}
