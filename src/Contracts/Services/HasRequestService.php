<?php

namespace Smorken\Service\Contracts\Services;

interface HasRequestService
{
    public function getRequestService(): ?RequestService;

    public function setRequestService(RequestService $requestService): void;
}
