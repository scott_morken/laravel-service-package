<?php

namespace Smorken\Service\Contracts\Services;

interface HasSanitizeService
{
    public function getSanitizeService(): ?SanitizeService;

    public function setSanitizeService(SanitizeService $service): void;
}
