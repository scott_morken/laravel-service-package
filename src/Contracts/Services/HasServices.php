<?php

namespace Smorken\Service\Contracts\Services;

interface HasServices
{
    public function getService(string $key): ?BaseService;

    public function getServices(): array;

    public function setService(string $key, BaseService $service): void;

    public function setServices(array $services): void;
}
