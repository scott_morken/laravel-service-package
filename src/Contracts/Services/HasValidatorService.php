<?php

namespace Smorken\Service\Contracts\Services;

interface HasValidatorService
{
    public function getValidatorService(): ?ValidatorService;

    public function setValidatorService(ValidatorService $service): void;
}
