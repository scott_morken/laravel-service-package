<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\IndexResult;

interface IndexService
{
    public function getByRequest(Request $request): IndexResult;
}
