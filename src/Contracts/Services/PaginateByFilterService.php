<?php

namespace Smorken\Service\Contracts\Services;

use Smorken\Service\Contracts\Services\VO\IndexResult;
use Smorken\Support\Contracts\Filter;

interface PaginateByFilterService extends HasFilterService, IndexService, StorageProviderService
{
    public function getByFilter(Filter $filter, int $perPage = 20): IndexResult;
}
