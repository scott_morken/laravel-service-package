<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Http\Request;

interface RequestService extends BaseService, HasSanitizeService
{
    public function getRequest(): Request;

    public function newInstance(Request $request): static;

    public function setRequest(Request $request): void;
}
