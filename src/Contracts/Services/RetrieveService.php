<?php

namespace Smorken\Service\Contracts\Services;

use Smorken\Service\Contracts\Enums\RetrieveTypes;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Contracts\Services\VO\VOResult;

interface RetrieveService extends BaseService
{
    /**
     * @param  RetrieveTypes::*  $type
     */
    public function findById(
        int|string $id,
        bool $shouldFail = true,
        string $type = RetrieveTypes::VIEW
    ): VOResult|ModelResult;
}
