<?php

namespace Smorken\Service\Contracts\Services;

interface RetrieveServiceByStorageProviderService extends RetrieveService, StorageProviderService {}
