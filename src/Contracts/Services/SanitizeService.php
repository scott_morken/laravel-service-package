<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Http\Request;
use Smorken\Sanitizer\Contracts\Sanitize;

interface SanitizeService extends BaseService
{
    public function getSanitizer(): Sanitize;

    public function sanitizeArray(array $array): array;

    public function sanitizeRequest(Request $request): Request;
}
