<?php

namespace Smorken\Service\Contracts\Services;

use Smorken\Model\Contracts\Model;
use Smorken\Storage\Contracts\Base as StorageProvider;

interface StorageProviderService extends BaseService, HasGateService
{
    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function authorize(?Model $model = null): bool;

    public function getProvider(): StorageProvider;

    public function setProvider(StorageProvider $provider): void;
}
