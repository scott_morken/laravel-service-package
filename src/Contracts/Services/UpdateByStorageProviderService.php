<?php

namespace Smorken\Service\Contracts\Services;

use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Contracts\Services\VO\VOResult;

interface UpdateByStorageProviderService extends StorageProviderService, UpdateService
{
    public function updateFromModel(\Smorken\Model\Contracts\Model $model, array $attributes): VOResult|ModelResult;
}
