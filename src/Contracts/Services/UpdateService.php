<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Contracts\Services\VO\VOResult;

interface UpdateService extends BaseService, HasRequestService, HasValidatorService
{
    public function getAttributesFromRequest(Request $request): array;

    public function updateFromId(int|string $id, array $attributes): VOResult|ModelResult;

    public function updateFromRequest(Request $request, int|string $id): VOResult|ModelResult;
}
