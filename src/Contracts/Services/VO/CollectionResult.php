<?php

namespace Smorken\Service\Contracts\Services\VO;

use Illuminate\Contracts\Support\MessageBag;

/**
 * @property \Illuminate\Support\Collection $collection
 * @property \Illuminate\Contracts\Support\MessageBag|null $messageBag
 *
 * @phpstan-require-extends \Smorken\Service\Services\VO\CollectionResult
 */
interface CollectionResult extends HasMessages, VOResult
{
    public function getMessageBag(): MessageBag;
}
