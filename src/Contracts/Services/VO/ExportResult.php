<?php

namespace Smorken\Service\Contracts\Services\VO;

/**
 * @property \Smorken\Export\Contracts\Export $exporter
 * @property \Smorken\Support\Contracts\Filter $filter
 *
 * @phpstan-require-extends \Smorken\Service\Services\VO\ExportResult
 */
interface ExportResult extends VOResult {}
