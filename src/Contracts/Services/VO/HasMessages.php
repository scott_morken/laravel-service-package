<?php

namespace Smorken\Service\Contracts\Services\VO;

/**
 * @property \Smorken\Service\Contracts\Services\VO\Message[] $messages
 */
interface HasMessages
{
    public function addMessage(string|Message $message, ?string $key = null): static;

    public function addMessages(array $messages): static;

    public function messagesToArray(): array;
}
