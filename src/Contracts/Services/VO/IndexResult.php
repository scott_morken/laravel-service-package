<?php

namespace Smorken\Service\Contracts\Services\VO;

/**
 * @property \Illuminate\Support\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator $models
 * @property \Smorken\Support\Contracts\Filter|null $filter
 *
 * @phpstan-require-extends \Smorken\Service\Services\VO\IndexResult
 */
interface IndexResult {}
