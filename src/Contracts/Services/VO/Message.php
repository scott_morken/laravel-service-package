<?php

namespace Smorken\Service\Contracts\Services\VO;

/**
 * @property string $message
 * @property ?string $key
 *
 * @phpstan-require-extends \Smorken\Service\Services\VO\Message
 */
interface Message
{
    public function __toString(): string;
}
