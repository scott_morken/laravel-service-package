<?php

namespace Smorken\Service\Contracts\Services\VO;

use Illuminate\Contracts\Support\MessageBag;

/**
 * @property \Smorken\Model\Contracts\Model $model
 * @property int|string|null $id
 * @property bool $result
 * @property \Illuminate\Contracts\Support\MessageBag|null $messageBag
 *
 * @phpstan-require-extends \Smorken\Service\Services\VO\ModelResult
 */
interface ModelResult extends HasMessages, VOResult
{
    public function getMessageBag(): MessageBag;
}
