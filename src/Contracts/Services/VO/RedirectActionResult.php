<?php

namespace Smorken\Service\Contracts\Services\VO;

use Illuminate\Http\RedirectResponse;

/**
 * @property array|string $action
 * @property array $params
 * @property \Illuminate\Contracts\Support\MessageBag|null $errors
 *
 * @phpstan-require-extends \Smorken\Service\Services\VO\RedirectActionResult
 */
interface RedirectActionResult extends VOResult
{
    public function setControllerClass(string $controllerClass): void;

    public function redirect(): RedirectResponse;
}
