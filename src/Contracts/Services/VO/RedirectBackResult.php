<?php

namespace Smorken\Service\Contracts\Services\VO;

use Illuminate\Http\RedirectResponse;

/**
 * @property bool $withInput
 * @property \Illuminate\Contracts\Support\MessageBag|null $errors
 *
 * @phpstan-require-extends \Smorken\Service\Services\VO\RedirectBackResponse
 */
interface RedirectBackResult
{
    public function redirect(): RedirectResponse;
}
