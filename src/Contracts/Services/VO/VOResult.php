<?php

namespace Smorken\Service\Contracts\Services\VO;

/**
 * @phpstan-require-extends \Smorken\Service\Services\VO\VOResult
 */
interface VOResult
{
    public function getAttributes(): array;

    public function newInstance(array $attributes = []): static;

    public function setAttributes(array $attributes): void;
}
