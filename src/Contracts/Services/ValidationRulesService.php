<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Enums\SaveTypes;

interface ValidationRulesService extends BaseService
{
    public function getBaseRules(): array;

    /**
     * @param  SaveTypes::*  $saveType
     */
    public function getRules(
        string $saveType,
        ?Request $request = null
    ): array;

    public function setBaseRules(array $rules): self;
}
