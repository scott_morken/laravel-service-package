<?php

namespace Smorken\Service\Contracts\Services;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

interface ValidatorService extends BaseService, HasRequestService
{
    public function attributes(): array;

    public function fromData(array $data, array $rules): Validator;

    public function fromRequest(Request $request, array $rules): Validator;

    public function getValidationFactory(): ?Factory;

    public function getValidator(array $data, array $rules, array $messages = [], array $attributes = []): Validator;

    public function messages(): array;

    public function rules(array $rules): array;

    public function setValidationFactory(Factory $validationFactory): void;

    public function validate(array $data, array $rules): bool;

    public function validateRequest(Request $request, array $rules): bool;

    public function validationDataFromRequest(Request $request): array;
}
