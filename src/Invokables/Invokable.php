<?php

namespace Smorken\Service\Invokables;

use Illuminate\Contracts\Foundation\Application;

abstract class Invokable implements \Smorken\Service\Contracts\Invokables\Invokable
{
    public function __construct(protected Application $app) {}

    public function getApp(): Application
    {
        return $this->app;
    }
}
