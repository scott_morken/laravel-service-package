<?php

namespace Smorken\Service\Invokables;

use Illuminate\Contracts\Foundation\Application;
use Smorken\Service\Invokables\Invokers\FromArray;
use Smorken\Service\Invokables\Invokers\FromFilesystem;

class Invoker implements \Smorken\Service\Contracts\Invokables\Invoker
{
    public function __construct(protected array $config, protected string $basePath) {}

    public function getBasePath(): string
    {
        return $this->basePath;
    }

    public function getConfigItem(string $key, mixed $default = null): mixed
    {
        return $this->config[$key] ?? $default;
    }

    public function handle(Application $app): void
    {
        $this->handleAutoload($app);
        $this->handleArray($app);
    }

    protected function getPath(): string
    {
        $part = $this->getConfigItem('path');
        if (! $part) {
            throw new InvokableServiceException('Path is not set.');
        }
        $realpath = realpath($this->getBasePath().DIRECTORY_SEPARATOR.$part);
        if (! $realpath) {
            throw new InvokableServiceException('Path does not exist.');
        }

        return $realpath;
    }

    protected function handleArray(Application $app): void
    {
        $services = $this->getConfigItem('invokables', []);
        $handler = new FromArray($services);
        $handler->handle($app);
    }

    protected function handleAutoload(Application $app): void
    {
        if ($this->getConfigItem('autoload', false)) {
            $handler = new FromFilesystem($this->getPath());
            $handler->handle($app);
        }
    }
}
