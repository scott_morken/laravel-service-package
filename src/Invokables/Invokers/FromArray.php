<?php

namespace Smorken\Service\Invokables\Invokers;

class FromArray extends HandlerBase implements \Smorken\Service\Contracts\Invokables\Invokers\FromArray
{
    public function __construct(protected array $classes) {}

    public function getClasses(): array
    {
        return $this->classes;
    }

    protected function getIterables(): array
    {
        return $this->getClasses();
    }
}
