<?php

namespace Smorken\Service\Invokables\Invokers;

use Smorken\Service\Invokables\InvokableServiceException;
use Smorken\Service\Invokables\TokenParser;

class FromFilesystem extends HandlerBase implements \Smorken\Service\Contracts\Invokables\Invokers\FromFilesystem
{
    protected string $path;

    public function __construct(string $path)
    {
        $this->setPath($path);
    }

    public function getPath(): string
    {
        return $this->path;
    }

    protected function setPath(string $path): void
    {
        $realpath = realpath($path);
        if (! $realpath) {
            throw new InvokableServiceException("$path does not exist.");
        }
        $this->path = $realpath;
    }

    protected function getClassFromFileInfo(\SplFileInfo $fileInfo): ?string
    {
        if ($fileInfo->isFile() && $fileInfo->getExtension() === 'php') {
            return TokenParser::getClass($fileInfo->getRealPath());
        }

        return null;
    }

    protected function getFiles(): array
    {
        $files = [];
        /**
         * @var \SplFileInfo $fileInfo
         */
        foreach (new \DirectoryIterator($this->getPath()) as $fileInfo) {
            $className = $this->getClassFromFileInfo($fileInfo);
            if ($className) {
                $files[] = $className;
            }
        }

        return $files;
    }

    protected function getIterables(): array
    {
        return $this->getFiles();
    }
}
