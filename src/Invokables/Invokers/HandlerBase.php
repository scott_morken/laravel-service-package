<?php

namespace Smorken\Service\Invokables\Invokers;

use Illuminate\Contracts\Foundation\Application;
use Smorken\Service\Contracts\Invokables\Invokable;
use Smorken\Service\Contracts\Invokables\Invokers\Handler;

abstract class HandlerBase implements Handler
{
    abstract protected function getIterables(): array;

    public function handle(Application $app): array
    {
        $handled = [];
        foreach ($this->getIterables() as $class) {
            $reflectionClass = new \ReflectionClass($class);
            if ($this->verify($reflectionClass)) {
                /**
                 * @var Invokable $f
                 */
                $f = $reflectionClass->newInstance($app);
                $f();
                $handled[] = $class;
            }
        }

        return $handled;
    }

    protected function verify(\ReflectionClass $reflectionClass): bool
    {
        return $reflectionClass->implementsInterface(Invokable::class);
    }
}
