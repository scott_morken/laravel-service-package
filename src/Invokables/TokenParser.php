<?php

namespace Smorken\Service\Invokables;

/**
 * @link https://stackoverflow.com/questions/7153000/get-class-name-from-file/44654073
 */
class TokenParser implements \Smorken\Service\Contracts\Invokables\TokenParser
{
    public static function getClass(string $file): ?string
    {
        return self::getClasses($file)[0] ?? null;
    }

    public static function getClasses(string $file): array
    {
        $classes = [];
        $namespace = '';
        if (! file_exists($file)) {
            return [];
        }
        $tokens = \PhpToken::tokenize(file_get_contents($file));
        for ($i = 0; $i < count($tokens); $i++) {
            if ($tokens[$i]->getTokenName() === 'T_NAMESPACE') {
                for ($j = $i + 1; $j < count($tokens); $j++) {
                    if ($tokens[$j]->getTokenName() === 'T_NAME_QUALIFIED') {
                        $namespace = $tokens[$j]->text;
                        break;
                    }
                }
            }
            if ($tokens[$i]->getTokenName() === 'T_CLASS') {
                for ($j = $i + 1; $j < count($tokens); $j++) {
                    if ($tokens[$j]->getTokenName() === 'T_WHITESPACE') {
                        continue;
                    }

                    if ($tokens[$j]->getTokenName() === 'T_STRING') {
                        $classes[] = $namespace.'\\'.$tokens[$j]->text;
                    } else {
                        break;
                    }
                }
            }
        }

        return $classes;
    }
}
