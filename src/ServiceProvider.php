<?php

namespace Smorken\Service;

use Smorken\Service\Invokables\Invoker;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfigs();
    }

    public function register(): void
    {
        $this->registerCoreServices();
        $this->handleInvokables();
    }

    protected function bootConfigs(): void
    {
        $config = __DIR__.'/../config/core.php';
        $this->mergeConfigFrom($config, 'servicescore');
        $this->publishes([$config => config_path('servicescore.php')], 'config');
        $config = __DIR__.'/../config/servicesinvokable.php';
        $this->mergeConfigFrom($config, 'servicesinvokable');
        $this->publishes([$config => config_path('servicesinvokable.php')], 'config');
    }

    protected function handleInvokables(): void
    {
        $config = $this->app['config']->get('servicesinvokable', []);
        $invoker = new Invoker($config, app_path());
        $invoker->handle($this->app);
    }

    protected function registerCoreServices(): void
    {
        $this->registerServicesFromArray($this->app['config']->get('servicescore', []));
    }

    protected function registerServicesFromArray(array $services): void
    {
        foreach ($services as $interface => $impl) {
            $this->app->bind($interface, fn ($app) => $app[$impl]);
        }
    }
}
