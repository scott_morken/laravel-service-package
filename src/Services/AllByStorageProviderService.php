<?php

namespace Smorken\Service\Services;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\IndexResult;

class AllByStorageProviderService extends StorageProviderService implements \Smorken\Service\Contracts\Services\AllByStorageProviderService
{
    protected string $voClass = \Smorken\Service\Services\VO\IndexResult::class;

    public function getByRequest(Request $request): IndexResult
    {
        $models = $this->getProvider()->all();

        /** @var IndexResult */
        return $this->newVO(['models' => $models]);
    }
}
