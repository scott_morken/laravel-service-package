<?php

namespace Smorken\Service\Services;

use Smorken\Service\Services\Traits\HasServicesTrait;
use Smorken\Service\Services\VO\VOResult;

abstract class BaseService implements \Smorken\Service\Contracts\Services\BaseService
{
    use HasServicesTrait;

    protected string $voClass = VOResult::class;

    public function __construct(array $services = [])
    {
        $this->setServices($services);
    }

    public function implements(string $interface): bool
    {
        return in_array($interface, class_implements($this));
    }

    public function newVO(array $params): \Smorken\Service\Contracts\Services\VO\VOResult
    {
        return new $this->voClass(...$params);
    }

    public function setVOClass(string $voClass): void
    {
        $this->voClass = $voClass;
    }
}
