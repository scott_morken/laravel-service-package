<?php

namespace Smorken\Service\Services;

use Illuminate\Http\Request;
use Smorken\Model\Contracts\Model;
use Smorken\Service\Contracts\Enums\GateBaseNames;
use Smorken\Service\Contracts\Enums\SaveTypes;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\Traits\HasMessagesTrait;
use Smorken\Service\Services\Traits\HasRequestServiceTrait;

class CreateByStorageProviderService extends ValidatesByStorageProviderService implements \Smorken\Service\Contracts\Services\CreateServiceByStorageProviderService
{
    use HasMessagesTrait, HasRequestServiceTrait;

    protected string $gateBaseName = GateBaseNames::CREATE;

    protected ?Request $request = null;

    public function createFromAttributes(array $attributes): ModelResult
    {
        $this->authorize([$attributes]);
        $attributes = $this->preCreate($attributes);
        $model = $this->getProvider()->create($attributes);
        $model = $this->postCreate($model, $attributes);

        return new \Smorken\Service\Services\VO\ModelResult(
            $model,
            $this->getIdFromModel($model),
            (bool) $model,
            $this->getMessages()
        );
    }

    public function createFromRequest(Request $request): ModelResult
    {
        $request = $this->getRequestFromRequestService($request);
        $this->request = $request;
        $this->validateRequest($request, SaveTypes::CREATE);
        $attributes = $this->getAttributesFromRequest($request);
        $attributes = $this->preCreateWithRequest($request, $attributes);

        return $this->createFromAttributes($attributes);
    }

    public function getAttributesFromRequest(Request $request): array
    {
        return $request->except(['_token']);
    }

    public function getModel(): ?Model
    {
        return $this->getProvider()->getModel();
    }

    protected function getRequest(): ?Request
    {
        return $this->request;
    }

    protected function postCreate(?Model $model, array $attributes): ?Model
    {
        return $model;
    }

    protected function preCreate(array $attributes): array
    {
        return $attributes;
    }

    protected function preCreateWithRequest(Request $request, array $attributes): array
    {
        return $attributes;
    }
}
