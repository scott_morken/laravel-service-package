<?php

namespace Smorken\Service\Services;

use Smorken\Service\Contracts\Services\CreateService;
use Smorken\Service\Contracts\Services\DeleteService;
use Smorken\Service\Contracts\Services\RetrieveService;
use Smorken\Service\Contracts\Services\UpdateService;
use Smorken\Storage\Contracts\Base;

class CrudByStorageProviderServices extends CrudServices implements \Smorken\Service\Contracts\Services\CrudByStorageProviderServices
{
    public static function createByStorageProvider(
        Base $provider,
        array $additionalServices = []
    ): static {
        $services = [
            CreateService::class => new \Smorken\Service\Services\CreateByStorageProviderService($provider),
            DeleteService::class => new \Smorken\Service\Services\DeleteByStorageProviderService($provider),
            RetrieveService::class => new \Smorken\Service\Services\RetrieveByStorageProviderService($provider),
            UpdateService::class => new \Smorken\Service\Services\UpdateByStorageProviderService($provider),
        ];

        // @phpstan-ignore new.static
        return new static($services, $additionalServices);
    }
}
