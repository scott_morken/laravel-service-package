<?php

namespace Smorken\Service\Services;

use Smorken\Service\Contracts\Services\CreateService;
use Smorken\Service\Contracts\Services\DeleteService;
use Smorken\Service\Contracts\Services\RetrieveService;
use Smorken\Service\Contracts\Services\UpdateService;

class CrudServices extends Factory implements \Smorken\Service\Contracts\Services\CrudServices
{
    protected array $services = [
        CreateService::class => null,
        DeleteService::class => null,
        RetrieveService::class => null,
        UpdateService::class => null,
    ];

    public function __construct(
        array $services,
        array $additionalServices = []
    ) {
        parent::__construct($services, $additionalServices);
    }

    public function getCreateService(): CreateService
    {
        /** @var CreateService */
        return $this->getService(CreateService::class);
    }

    public function getDeleteService(): DeleteService
    {
        /** @var DeleteService */
        return $this->getService(DeleteService::class);
    }

    public function getRetrieveService(): RetrieveService
    {
        /** @var RetrieveService */
        return $this->getService(RetrieveService::class);
    }

    public function getServices(): array
    {
        return $this->services;
    }

    public function setServices(array $services): void
    {
        foreach ($services as $iface => $service) {
            $this->setService($iface, $service);
        }
    }

    public function getUpdateService(): UpdateService
    {
        /** @var UpdateService */
        return $this->getService(UpdateService::class);
    }

    public function setCreateService(CreateService $providerService): void
    {
        $this->setService(CreateService::class, $providerService);
    }

    public function setDeleteService(DeleteService $providerService): void
    {
        $this->setService(DeleteService::class, $providerService);
    }

    public function setRetrieveService(RetrieveService $providerService): void
    {
        $this->setService(RetrieveService::class, $providerService);
    }

    public function setUpdateService(UpdateService $providerService): void
    {
        $this->setService(UpdateService::class, $providerService);
    }
}
