<?php

namespace Smorken\Service\Services;

use Illuminate\Http\Request;
use Smorken\Model\Contracts\Model;
use Smorken\Service\Contracts\Enums\GateBaseNames;
use Smorken\Service\Contracts\Services\VO\ModelResult;

class DeleteByStorageProviderService extends StorageProviderService implements \Smorken\Service\Contracts\Services\DeleteServiceByStorageProviderService
{
    protected string $gateBaseName = GateBaseNames::DELETE;

    public function canDelete(Model $model): bool
    {
        return $this->authorize($model) && $this->deleteAllowedForModel($model);
    }

    public function deleteFromId(int|string $id): ModelResult
    {
        $model = $this->findModelById($id);

        return $this->deleteFromModel($model);
    }

    public function deleteFromModel(Model $model): ModelResult
    {
        $deleted = false;
        $message = [];
        $id = $this->getIdFromModel($model);
        if ($this->canDelete($model)) {
            $this->preDelete($model);
            $deleted = $this->getProvider()->delete($model);
            $this->postDelete($model, $deleted);
        } else {
            $message['flash:danger'][] = 'Delete not allowed for ['.$id.'].';
        }

        return new \Smorken\Service\Services\VO\ModelResult($model, $id, $deleted, $message);
    }

    public function deleteFromRequest(Request $request, int|string $id): ModelResult
    {
        $this->preDeleteWithRequest($request, $id);

        return $this->deleteFromId($id);
    }

    protected function deleteAllowedForModel(Model $model): bool
    {
        return true;
    }

    protected function postDelete(Model $model, bool $deleted): void
    {
        // override if needed
    }

    protected function preDelete(Model $model): void
    {
        // override if needed
    }

    protected function preDeleteWithRequest(Request $request, int|string $id): void
    {
        // override if needed
    }
}
