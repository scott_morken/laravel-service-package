<?php

namespace Smorken\Service\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\Export\Contracts\Export;
use Smorken\Service\Contracts\Services\VO\ExportResult;
use Smorken\Service\Services\Traits\HasFilterServiceTrait;
use Smorken\Storage\Contracts\Base as StorageProvider;
use Smorken\Support\Contracts\Filter;

class ExportByStorageProviderService extends StorageProviderService implements \Smorken\Service\Contracts\Services\ExportByStorageProviderService
{
    use HasFilterServiceTrait;

    protected bool $headerHandled = false;

    protected string $voClass = \Smorken\Service\Services\VO\ExportResult::class;

    public function __construct(protected Export $exporter, StorageProvider $provider, array $services = [])
    {
        parent::__construct($provider, $services);
    }

    public function export(Request $request): ExportResult
    {
        $this->getExporter()->init($this->getHeaderCallback(), $this->getRowCallback());
        $filter = $this->getFilter($request);
        $this->processModels($request, $filter);

        /** @var ExportResult */
        return $this->newVO(['exporter' => $this->getExporter(), 'filter' => $filter]);
    }

    public function getExporter(): Export
    {
        return $this->exporter;
    }

    public function setExporter(Export $exporter): void
    {
        $this->exporter = $exporter;
    }

    protected function getFilter(Request $request): Filter
    {
        return $this->modifyFilter(
            $request,
            ($this->getFilterService()?->getFilterFromRequest($request) ?? new \Smorken\Support\Filter)
        );
    }

    protected function getHeaderCallback(...$params): ?callable
    {
        return null;
    }

    protected function getRowCallback(...$params): ?callable
    {
        return null;
    }

    protected function handleHeader(mixed $models): void
    {
        if (! $this->headerHandled) {
            $this->getExporter()->header($models);
            $this->headerHandled = true;
        }
    }

    protected function modifyFilter(Request $request, Filter $filter): Filter
    {
        return $filter;
    }

    protected function modifyModels(Collection $models): Collection
    {
        return $models;
    }

    protected function processModels(Request $request, Filter $filter): void
    {
        $this->getProvider()->chunkFilter($filter, function ($models) {
            $models = $this->modifyModels($models);
            $this->handleHeader($models);
            $this->getExporter()->process($models);
        });
    }
}
