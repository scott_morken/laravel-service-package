<?php

namespace Smorken\Service\Services;

use Smorken\Service\Contracts\Services\BaseService;

class Factory implements \Smorken\Service\Contracts\Services\Factory
{
    protected array $services = [];

    public function __construct(array $services, protected array $additionalServices = [])
    {
        $this->setServices($services);
    }

    public function getService(string $name): BaseService
    {
        return $this->services[$name];
    }

    public function getServices(): array
    {
        return $this->services;
    }

    public function setServices(array $services): void
    {
        foreach ($services as $name => $service) {
            $this->setService($name, $service);
        }
    }

    public function setService(string $name, BaseService $service): void
    {
        if (! array_key_exists($name, $this->services)) {
            throw new \OutOfBoundsException("[$name] does not exist.");
        }
        $this->services[$name] = $service;
        $this->additionalSetServiceActions($name, $service);
    }

    protected function additionalSetServiceActions(string $name, BaseService $service): void
    {
        $service->setServices($this->additionalServices);
    }
}
