<?php

namespace Smorken\Service\Services;

use Illuminate\Http\Request;
use Smorken\Service\Services\Traits\HasRequestServiceTrait;
use Smorken\Service\Services\Traits\HasValidatorServiceTrait;
use Smorken\Support\Contracts\Filter;

class FilterService extends BaseService implements \Smorken\Service\Contracts\Services\FilterService
{
    use HasRequestServiceTrait, HasValidatorServiceTrait;

    public function getFilterFromRequest(Request $request): Filter
    {
        $request = $this->getRequestFromRequestService($request);
        $this->validateRequest($request);

        return $this->createFilterFromRequest($request);
    }

    /**
     * Override this method in your class
     */
    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter;
    }

    protected function getRules(): array
    {
        return [];
    }

    protected function validateRequest(Request $request): bool
    {
        $validator = $this->getValidatorService();

        return $validator?->validateRequest($request, $this->getRules()) ?? true;
    }
}
