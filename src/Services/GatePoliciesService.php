<?php

namespace Smorken\Service\Services;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Service\Contracts\Enums\GateBaseNames;

class GatePoliciesService extends BaseService implements \Smorken\Service\Contracts\Services\GatePoliciesService
{
    use HandlesAuthorization;

    protected ?Gate $gate = null;

    protected array $methods = [
        GateBaseNames::CREATE => 'create',
        GateBaseNames::DELETE => 'delete',
        GateBaseNames::DESTROY => 'destroy',
        GateBaseNames::FORCE_DELETE => 'forceDelete',
        GateBaseNames::INDEX => 'index',
        GateBaseNames::RESTORE => 'restore',
        GateBaseNames::UPDATE => 'update',
        GateBaseNames::VIEW => 'view',
        GateBaseNames::VIEW_ANY => 'viewAny',
    ];

    protected string $modelClass = '';

    public function after(Authenticatable $user, $ability): ?bool
    {
        return null;
    }

    public function before(Authenticatable $user, $ability): ?bool
    {
        return null;
    }

    public function create(Authenticatable $user, ?array $attributes = null): bool|Response
    {
        return $this->isUserAllowedToCreate($user, $attributes);
    }

    public function delete(Authenticatable $user, mixed $model): bool|Response
    {
        return $this->destroy($user, $model);
    }

    public function destroy(Authenticatable $user, mixed $model): bool|Response
    {
        return $this->isUserAllowedToDestroy($user, $model);
    }

    public function forceDelete(Authenticatable $user, mixed $model): bool|Response
    {
        return $this->destroy($user, $model);
    }

    public function getAbilityToMethodMap(): array
    {
        $map = [];
        foreach ($this->methods as $ability => $methodName) {
            $map[$this->mixModelWithMethod($ability)] = $methodName;
        }

        return $map;
    }

    public function getGate(): Gate
    {
        return $this->gate;
    }

    public function setGate(Gate $gate): void
    {
        $this->gate = $gate;
    }

    public function getModelClass(): string
    {
        return $this->modelClass;
    }

    public function index(Authenticatable $user): bool|Response
    {
        return $this->viewAny($user);
    }

    public function restore(Authenticatable $user, mixed $model): bool|Response
    {
        return $this->destroy($user, $model);
    }

    public function update(Authenticatable $user, mixed $model): bool|Response
    {
        return $this->isUserAllowedToUpdate($user, $model);
    }

    public function view(Authenticatable $user, mixed $model): bool|Response
    {
        return $this->isUserAllowedToView($user, $model);
    }

    public function viewAny(Authenticatable $user): bool|Response
    {
        return $this->isUserAllowedToViewAny($user);
    }

    protected function isUserAllowedToCreate(Authenticatable $user, ?array $attributes = null): Response
    {
        return $this->deny('You do not have permission to create a new record.');
    }

    protected function isUserAllowedToDestroy(Authenticatable $user, mixed $model): Response
    {
        return $this->deny('You do not have permission to delete this record.');
    }

    protected function isUserAllowedToUpdate(Authenticatable $user, mixed $model): Response
    {
        return $this->deny('You do not have permission to update this record.');
    }

    protected function isUserAllowedToView(Authenticatable $user, mixed $model): Response
    {
        return $this->deny('You do not have permission to view this record.');
    }

    protected function isUserAllowedToViewAny(Authenticatable $user): Response
    {
        return $this->deny('You do not have permission to view these requests.');
    }

    protected function mixModelWithMethod(string $ability): string
    {
        if ($this->modelClass) {
            return class_basename($this->modelClass).'.'.$ability;
        }

        return $ability;
    }
}
