<?php

namespace Smorken\Service\Services;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;

class GateService extends BaseService implements \Smorken\Service\Contracts\Services\GateService
{
    public function __construct(protected Gate $gate, array $services = [])
    {
        parent::__construct($services);
    }

    public function check(string $gateName, mixed $args = null, bool $throwException = true): bool
    {
        if ($throwException) {
            $response = $this->getGate()->authorize($gateName, $args);

            return $response->allowed();
        }

        return $this->getGate()->allows($gateName, $args);
    }

    public function checkRequest(
        Request $request,
        string $gateName,
        mixed $args = null,
        bool $throwException = true
    ): bool {
        return $this->checkUser($request->user(), $gateName, $args, $throwException);
    }

    public function checkUser(
        Authenticatable $user,
        string $gateName,
        mixed $args = null,
        bool $throwsException = true
    ): bool {
        if ($throwsException) {
            $response = $this->getGate()->forUser($user)->authorize($gateName, $args);

            return $response->allowed();
        }

        return $this->getGate()->forUser($user)->allows($gateName, $args);
    }

    public function getGate(): Gate
    {
        return $this->gate;
    }

    public function has(string $gateName): bool
    {
        return $this->getGate()->has($gateName);
    }

    public function setGate(Gate $gate): void
    {
        $this->gate = $gate;
    }
}
