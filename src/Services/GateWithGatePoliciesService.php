<?php

namespace Smorken\Service\Services;

use Illuminate\Contracts\Auth\Access\Gate;

class GateWithGatePoliciesService extends GateService implements \Smorken\Service\Contracts\Services\GateService
{
    public function __construct(Gate $gate, array $services = [])
    {
        parent::__construct($gate, $services);
        $this->initPolicies();
    }

    public function getGatePoliciesService(): \Smorken\Service\Contracts\Services\GatePoliciesService
    {
        /** @var \Smorken\Service\Contracts\Services\GatePoliciesService */
        return $this->getService(\Smorken\Service\Contracts\Services\GatePoliciesService::class);
    }

    protected function initPolicies(): void
    {
        $this->getGatePoliciesService()->setGate($this->getGate());
        $this->getGate()->before([$this->getGatePoliciesService(), 'before']);
        $this->getGate()->after([$this->getGatePoliciesService(), 'after']);
        foreach ($this->getGatePoliciesService()->getAbilityToMethodMap() as $ability => $method) {
            $this->getGate()->define($ability, [$this->getGatePoliciesService(), $method]);
        }
    }
}
