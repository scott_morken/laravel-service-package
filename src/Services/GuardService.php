<?php

namespace Smorken\Service\Services;

use Illuminate\Contracts\Auth\Guard;

class GuardService extends BaseService implements \Smorken\Service\Contracts\Services\GuardService
{
    public function __construct(protected Guard $guard, array $services = [])
    {
        parent::__construct($services);
    }

    public function getGuard(): Guard
    {
        return $this->guard;
    }

    public function setGuard(Guard $guard): void
    {
        $this->guard = $guard;
    }
}
