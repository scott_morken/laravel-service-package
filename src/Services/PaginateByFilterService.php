<?php

namespace Smorken\Service\Services;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Enums\GateBaseNames;
use Smorken\Service\Contracts\Services\VO\IndexResult;
use Smorken\Service\Services\Traits\HasFilterServiceTrait;
use Smorken\Storage\Contracts\Base as StorageProvider;
use Smorken\Support\Contracts\Filter;

class PaginateByFilterService extends StorageProviderService implements \Smorken\Service\Contracts\Services\PaginateByFilterService
{
    use HasFilterServiceTrait;

    protected string $gateBaseName = GateBaseNames::VIEW_ANY;

    protected string $voClass = \Smorken\Service\Services\VO\IndexResult::class;

    public function __construct(protected StorageProvider $provider, array $services = [])
    {
        parent::__construct($this->provider, $services);
    }

    public function getByFilter(Filter $filter, int $perPage = 20): IndexResult
    {
        $this->authorize();
        $models = $this->getProvider()->getByFilter($filter, $perPage);

        /** @var IndexResult */
        return $this->newVO([$models, $filter]);
    }

    public function getByRequest(Request $request): IndexResult
    {
        $filter = $this->getFilterService()->getFilterFromRequest($request);

        return $this->getByFilter($filter, $this->getPerPage($request));
    }

    protected function getPerPage(Request $request): int
    {
        return 20;
    }
}
