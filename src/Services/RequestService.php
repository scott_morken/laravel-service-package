<?php

namespace Smorken\Service\Services;

use Illuminate\Http\Request;
use Smorken\Service\Services\Traits\HasSanitizeServiceTrait;

class RequestService extends BaseService implements \Smorken\Service\Contracts\Services\RequestService
{
    use HasSanitizeServiceTrait;

    protected ?Request $request = null;

    public function __construct(?Request $request = null, array $services = [])
    {
        parent::__construct($services);
        if (! is_null($request)) {
            $this->setRequest($request);
        }
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function setRequest(Request $request): void
    {
        $request = $this->sanitizeRequest($request);
        $this->request = $this->modifyRequest($request);
    }

    public function newInstance(Request $request): static
    {
        // @phpstan-ignore new.static
        return new static($request, $this->getServices());
    }

    protected function modifyRequest(Request $request): Request
    {
        return $request;
    }

    protected function sanitizeRequest(Request $request): Request
    {
        return $this->getSanitizeService()?->sanitizeRequest($request) ?? $request;
    }
}
