<?php

namespace Smorken\Service\Services;

use Smorken\Service\Contracts\Enums\GateBaseNames;
use Smorken\Service\Contracts\Enums\RetrieveTypes;
use Smorken\Service\Services\VO\ModelResult;

class RetrieveByStorageProviderService extends StorageProviderService implements \Smorken\Service\Contracts\Services\RetrieveServiceByStorageProviderService
{
    protected string $voClass = ModelResult::class;

    /**
     * @param  RetrieveTypes::*  $type
     */
    public function findById(
        int|string $id,
        bool $shouldFail = true,
        string $type = RetrieveTypes::VIEW
    ): \Smorken\Service\Contracts\Services\VO\ModelResult {
        $this->updateGateBaseName($type);
        $model = $this->findModelById($id, $shouldFail);
        $this->authorize($model);

        /** @var \Smorken\Service\Contracts\Services\VO\ModelResult */
        return $this->newVO([$model, $this->getIdFromModel($model), (bool) $model]);
    }

    /**
     * @param  RetrieveTypes::*  $type
     */
    protected function updateGateBaseName(
        string $type = RetrieveTypes::VIEW
    ): void {
        if ($type === RetrieveTypes::UPDATE) {
            $this->gateBaseName = GateBaseNames::UPDATE;
        }
        if ($type === RetrieveTypes::DELETE) {
            $this->gateBaseName = GateBaseNames::DELETE;
        }
    }
}
