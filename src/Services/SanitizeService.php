<?php

namespace Smorken\Service\Services;

use Illuminate\Http\Request;
use Smorken\Sanitizer\Contracts\Sanitize;

abstract class SanitizeService extends BaseService implements \Smorken\Service\Contracts\Services\SanitizeService
{
    public function __construct(protected Sanitize $sanitize, array $services = [])
    {
        parent::__construct($services);
    }

    abstract protected function handleArraySanitizing(array $array): array;

    public function getSanitizer(): Sanitize
    {
        return $this->sanitize;
    }

    public function sanitizeArray(array $array): array
    {
        return $this->handleArraySanitizing($array);
    }

    public function sanitizeRequest(Request $request): Request
    {
        return $request->replace($this->sanitizeArray($this->getAttributesFromRequest($request)));
    }

    protected function getAttributesFromRequest(Request $request): array
    {
        return $request->all();
    }
}
