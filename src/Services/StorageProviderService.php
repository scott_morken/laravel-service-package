<?php

namespace Smorken\Service\Services;

use Smorken\Model\Contracts\Model;
use Smorken\Service\Contracts\Enums\GateBaseNames;
use Smorken\Service\Services\Traits\HasAuthorizeTrait;
use Smorken\Storage\Contracts\Base as StorageProvider;

class StorageProviderService extends BaseService implements \Smorken\Service\Contracts\Services\StorageProviderService
{
    use HasAuthorizeTrait;

    protected string $gateBaseName = GateBaseNames::VIEW;

    public function __construct(
        protected StorageProvider $provider,
        array $services = []
    ) {
        parent::__construct($services);
    }

    public function getProvider(): StorageProvider
    {
        return $this->provider;
    }

    public function setProvider(StorageProvider $provider): void
    {
        $this->provider = $provider;
    }

    protected function findModelById(int|string $id, bool $shouldFail = true): ?Model
    {
        if ($shouldFail) {
            return $this->getProvider()->findOrFail($id);
        }

        return $this->getProvider()->find($id);
    }

    protected function getIdFromModel(mixed $model): int|string|null
    {
        if (is_object($model) && method_exists($model, 'getKey')) {
            return $model->getKey();
        }

        return $model && $model->id ? $model->id : null;
    }
}
