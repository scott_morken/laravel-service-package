<?php

namespace Smorken\Service\Services\Traits;

trait HasAuthorizeTrait
{
    use HasGateServiceTrait;

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function authorize(mixed $model = null): bool
    {
        $gateName = $this->getGateName();
        if ($this->getGateService() && $this->getGateService()->has($gateName)) {
            return $this->getGateService()->check($gateName, $model);
        }

        return true;
    }

    protected function getGateBaseName(): string
    {
        if (property_exists($this, 'gateBaseName')) {
            return $this->gateBaseName;
        }

        return 'view';
    }

    protected function getGateModelClass(): ?string
    {
        return $this->getGateModelClassFromProperty() ?? $this->getGateModelClassFromProvider();
    }

    protected function getGateModelClassFromProperty(): ?string
    {
        if (property_exists($this, 'gateModelClass')) {
            return class_basename($this->gateModelClass);
        }

        return null;
    }

    protected function getGateModelClassFromProvider(): ?string
    {
        if (method_exists($this, 'getProvider')) {
            try {
                return class_basename($this->getProvider()->getModel());
            } catch (\Throwable) {
                // do nothing
            }
        }

        return null;
    }

    protected function getGateName(): string
    {
        $parts = [
            $this->getGateModelClass(),
            $this->getGateBaseName(),
        ];

        return implode('.', array_filter($parts));
    }
}
