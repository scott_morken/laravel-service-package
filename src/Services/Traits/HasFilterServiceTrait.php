<?php

namespace Smorken\Service\Services\Traits;

use Smorken\Service\Contracts\Services\FilterService;

trait HasFilterServiceTrait
{
    public function getFilterService(): ?FilterService
    {
        /** @var FilterService|null */
        return $this->getService(FilterService::class);
    }

    public function setFilterService(FilterService $filterService): void
    {
        $this->setService(FilterService::class, $filterService);
    }
}
