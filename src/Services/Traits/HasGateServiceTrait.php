<?php

namespace Smorken\Service\Services\Traits;

use Smorken\Service\Contracts\Services\GateService;

trait HasGateServiceTrait
{
    public function getGateService(): ?GateService
    {
        /** @var GateService|null */
        return $this->getService(GateService::class);
    }

    public function setGateService(GateService $gateService): void
    {
        $this->setService(GateService::class, $gateService);
    }

    protected function setGateServiceMutation(GateService $gateService): void
    {
        $this->services[GateService::class] = $gateService;
        $this->setupGates($gateService);
    }

    protected function setupGates(\Smorken\Service\Contracts\Services\GateService $gateService): void
    {
        // override if needed
    }
}
