<?php

namespace Smorken\Service\Services\Traits;

use Smorken\Service\Contracts\Services\GuardService;

trait HasGuardServiceTrait
{
    public function getGuardService(): ?GuardService
    {
        /** @var GuardService|null */
        return $this->getService(GuardService::class);
    }

    public function setGuardService(GuardService $guardService): void
    {
        $this->setService(GuardService::class, $guardService);
    }
}
