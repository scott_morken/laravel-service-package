<?php

namespace Smorken\Service\Services\Traits;

trait HasMessagesTrait
{
    protected array $messages = [];

    protected function addMessage(string $key, string $message): void
    {
        if (! ($this->getMessages()[$key] ?? null)) {
            $this->messages[$key] = [];
        }
        $this->messages[$key][] = $message;
    }

    protected function getMessages(): array
    {
        return $this->messages;
    }
}
