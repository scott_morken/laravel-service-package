<?php

namespace Smorken\Service\Services\Traits;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\RequestService;

trait HasRequestServiceTrait
{
    protected ?RequestService $requestService = null;

    public function getRequestService(): ?RequestService
    {
        /** @var RequestService|null */
        return $this->getService(RequestService::class);
    }

    public function setRequestService(RequestService $service): void
    {
        $this->setService(RequestService::class, $service);
    }

    protected function getRequestFromRequestService(Request $request): Request
    {
        if ($this->getRequestService()) {
            return $this->getRequestService()->newInstance($request)->getRequest();
        }

        return $request;
    }
}
