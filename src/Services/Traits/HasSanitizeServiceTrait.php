<?php

namespace Smorken\Service\Services\Traits;

use Smorken\Service\Contracts\Services\SanitizeService;

trait HasSanitizeServiceTrait
{
    public function getSanitizeService(): ?SanitizeService
    {
        /** @var SanitizeService|null */
        return $this->getService(SanitizeService::class);
    }

    public function setSanitizeService(SanitizeService $service): void
    {
        $this->setService(SanitizeService::class, $service);
    }
}
