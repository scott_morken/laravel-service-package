<?php

namespace Smorken\Service\Services\Traits;

use Smorken\Service\Contracts\Services\BaseService;

trait HasServicesTrait
{
    protected array $services = [];

    public function getService(string $key): ?BaseService
    {
        $getMutation = $this->getGetMutation($key);
        if ($getMutation) {
            return $this->$getMutation();
        }

        return $this->getServices()[$key] ?? null;
    }

    public function getServices(): array
    {
        return $this->services;
    }

    public function setServices(array $services): void
    {
        foreach ($services as $k => $s) {
            $this->setService($k, $s);
        }
    }

    public function setService(string $key, BaseService $service): void
    {
        $setMutation = $this->getSetMutation($key);
        if ($setMutation) {
            $this->$setMutation($service);
        } else {
            $this->services[$key] = $service;
        }
    }

    protected function getGetMutation(string $key): ?string
    {
        return $this->getMutation('get', $key);
    }

    protected function getMutation(string $type, string $key): ?string
    {
        $method = sprintf('%s%sMutation', $type, class_basename($key));
        if (method_exists($this, $method)) {
            return $method;
        }

        return null;
    }

    protected function getSetMutation(string $key): ?string
    {
        return $this->getMutation('set', $key);
    }
}
