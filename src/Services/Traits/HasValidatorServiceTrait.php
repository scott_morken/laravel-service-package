<?php

namespace Smorken\Service\Services\Traits;

use Illuminate\Contracts\Validation\Validator;
use Smorken\Service\Contracts\Services\ValidatorService;

trait HasValidatorServiceTrait
{
    protected ?ValidatorService $validatorService = null;

    public function getValidatorService(): ?ValidatorService
    {
        /** @var ValidatorService|null */
        return $this->getService(ValidatorService::class);
    }

    public function setValidatorService(ValidatorService $service): void
    {
        $this->setService(Validator::class, $service);
    }
}
