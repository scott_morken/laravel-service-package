<?php

namespace Smorken\Service\Services\Traits;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Enums\SaveTypes;
use Smorken\Service\Services\ValidationRulesByStorageProviderService;

trait ValidateByStorageProviderTrait
{
    /**
     * @param  SaveTypes::*  $saveType
     */
    protected function getRules(
        string $saveType,
        ?Request $request = null
    ): array {
        return $this->getValidationRulesService()->getRules($saveType, $request);
    }

    protected function getValidationRulesService(): \Smorken\Service\Contracts\Services\ValidationRulesService
    {
        /** @var \Smorken\Service\Contracts\Services\ValidationRulesService */
        return $this->getService(\Smorken\Service\Contracts\Services\ValidationRulesService::class) ?? new ValidationRulesByStorageProviderService($this->getProvider());
    }

    /**
     * @param  SaveTypes::*  $saveType
     */
    protected function validateData(
        array $data,
        string $saveType
    ): bool {
        $v = $this->getValidatorService();

        return $v?->validate($data, $this->getRules($saveType)) ?? true;
    }

    /**
     * @param  SaveTypes::*  $saveType
     */
    protected function validateRequest(
        Request $request,
        string $saveType
    ): bool {
        $v = $this->getValidatorService();

        return $v?->validateRequest($request, $this->getRules($saveType, $request)) ?? true;
    }
}
