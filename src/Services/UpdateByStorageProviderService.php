<?php

namespace Smorken\Service\Services;

use Illuminate\Http\Request;
use Smorken\Model\Contracts\Model;
use Smorken\Service\Contracts\Enums\GateBaseNames;
use Smorken\Service\Contracts\Enums\SaveTypes;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\Traits\HasMessagesTrait;
use Smorken\Service\Services\Traits\HasRequestServiceTrait;

class UpdateByStorageProviderService extends ValidatesByStorageProviderService implements \Smorken\Service\Contracts\Services\UpdateByStorageProviderService
{
    use HasMessagesTrait, HasRequestServiceTrait;

    protected string $gateBaseName = GateBaseNames::UPDATE;

    protected ?Request $request = null;

    public function getAttributesFromRequest(Request $request): array
    {
        return $request->except(['_token']);
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateFromId(int|string $id, array $attributes): ModelResult
    {
        return $this->updateFromModel($this->findModelById($id), $attributes);
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateFromModel(\Smorken\Model\Contracts\Model $model, array $attributes): ModelResult
    {
        $this->authorize($model);
        $attributes = $this->preUpdate($model, $attributes);
        $result = $this->getProvider()->update($model, $attributes);
        $model = $this->postUpdate($model, $attributes);

        return new \Smorken\Service\Services\VO\ModelResult(
            $model,
            $this->getIdFromModel($model),
            (bool) $result,
            $this->getMessages()
        );
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateFromRequest(Request $request, int|string $id): ModelResult
    {
        $request = $this->getRequestFromRequestService($request);
        $this->request = $request;
        $this->validateRequest($request, SaveTypes::UPDATE);
        $attributes = $this->getAttributesFromRequest($request);
        $attributes = $this->preUpdateWithRequest($request, $id, $attributes);

        return $this->updateFromId($id, $attributes);
    }

    protected function getRequest(): ?Request
    {
        return $this->request;
    }

    protected function postUpdate(Model $model, array $attributes): Model
    {
        return $model;
    }

    protected function preUpdate(Model $model, array $attributes): array
    {
        return $attributes;
    }

    protected function preUpdateWithRequest(Request $request, int|string $id, array $attributes): array
    {
        return $attributes;
    }
}
