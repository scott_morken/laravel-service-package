<?php

namespace Smorken\Service\Services\VO;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Support\Collection;
use Smorken\Service\Services\VO\Traits\HasMessages;

class CollectionResult extends VOResult implements \Smorken\Service\Contracts\Services\VO\CollectionResult
{
    use HasMessages;

    public function __construct(
        public Collection $collection,
        public ?MessageBag $messageBag = null
    ) {
        $this->getMessageBag();
    }

    public function getMessageBag(): MessageBag
    {
        if (! $this->messageBag) {
            $this->messageBag = new \Illuminate\Support\MessageBag;
        }

        return $this->messageBag;
    }
}
