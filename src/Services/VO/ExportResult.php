<?php

namespace Smorken\Service\Services\VO;

use Smorken\Export\Contracts\Export;
use Smorken\Support\Contracts\Filter;

class ExportResult extends VOResult implements \Smorken\Service\Contracts\Services\VO\ExportResult
{
    public function __construct(
        public Export $exporter,
        public Filter $filter
    ) {}
}
