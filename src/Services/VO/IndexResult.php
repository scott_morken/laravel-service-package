<?php

namespace Smorken\Service\Services\VO;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Smorken\Support\Contracts\Filter;

class IndexResult extends VOResult implements \Smorken\Service\Contracts\Services\VO\IndexResult
{
    public function __construct(public Collection|LengthAwarePaginator $models, public ?Filter $filter = null) {}
}
