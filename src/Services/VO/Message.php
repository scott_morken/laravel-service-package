<?php

namespace Smorken\Service\Services\VO;

class Message extends VOResult implements \Smorken\Service\Contracts\Services\VO\Message
{
    public function __construct(public string $message, public ?string $key = null) {}

    public function __toString(): string
    {
        if ($this->key) {
            return sprintf('%s: %s', $this->key, $this->message);
        }

        return $this->message;
    }
}
