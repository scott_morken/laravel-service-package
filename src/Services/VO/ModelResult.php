<?php

namespace Smorken\Service\Services\VO;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Contracts\Support\MessageProvider;
use Smorken\Service\Services\VO\Traits\HasMessages;

class ModelResult extends VOResult implements \Smorken\Service\Contracts\Services\VO\ModelResult
{
    use HasMessages;

    protected MessageBag|MessageProvider|null $messageBag = null;

    public function __construct(
        public ?\Smorken\Model\Contracts\Model $model,
        public mixed $id,
        public bool $result,
        array $messages = [],
        MessageBag|MessageProvider|null $messageBag = null
    ) {
        $this->addMessages($messages);
        $this->mergeMessageBag($messageBag);
    }

    public function getMessageBag(): MessageBag
    {
        if (is_null($this->messageBag)) {
            $this->messageBag = new \Illuminate\Support\MessageBag;
        }

        return $this->messageBag;
    }

    protected function mergeMessageBag(MessageBag|MessageProvider|null $messageBag): void
    {
        if ($messageBag) {
            $this->getMessageBag()->merge($messageBag);
        }
    }
}
