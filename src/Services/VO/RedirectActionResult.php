<?php

namespace Smorken\Service\Services\VO;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Smorken\Controller\View\WithService\CrudController;

class RedirectActionResult extends VOResult implements \Smorken\Service\Contracts\Services\VO\RedirectActionResult
{
    protected string $controllerClass = CrudController::class;

    public function __construct(
        public array|string $action,
        public array $params = [],
        public ?MessageBag $errors = null
    ) {}

    public function redirect(): RedirectResponse
    {
        $response = Redirect::action($this->action, $this->params);
        if ($this->errors && $this->errors->isNotEmpty()) {
            // @phpstan-ignore argument.type
            $response->withErrors($this->errors);
        }

        return $response;
    }

    public function setControllerClass(string $controllerClass): void
    {
        $this->controllerClass = $controllerClass;
    }
}
