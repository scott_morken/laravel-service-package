<?php

namespace Smorken\Service\Services\VO;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Smorken\Service\Contracts\Services\VO\RedirectBackResult;

class RedirectBackResponse extends VOResult implements RedirectBackResult
{
    public function __construct(public bool $withInput, public ?MessageBag $errors = null) {}

    public function redirect(): RedirectResponse
    {
        $response = Redirect::back();
        if ($this->withInput) {
            $response->withInput();
        }
        if ($this->errors) {
            // @phpstan-ignore argument.type
            $response->withErrors($this->errors);
        }

        return $response;
    }
}
