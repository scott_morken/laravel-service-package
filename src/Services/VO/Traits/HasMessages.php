<?php

namespace Smorken\Service\Services\VO\Traits;

use Smorken\Service\Services\VO\Message;

trait HasMessages
{
    /**
     * @var \Smorken\Service\Contracts\Services\VO\Message[]
     */
    public array $messages = [];

    public function addMessage(
        string|\Smorken\Service\Contracts\Services\VO\Message $message,
        ?string $key = null
    ): static {
        $this->messages[] = is_string($message) ? new Message($message, $key) : $message;

        return $this;
    }

    public function addMessages(array $messages, $key = null): static
    {
        foreach ($messages as $k => $message) {
            if (is_array($message)) {
                $this->addMessages($message, $this->getKey($k, $key));
            } else {
                $this->addMessage($message, $this->getKey($k, $key));
            }
        }

        return $this;
    }

    public function messagesToArray(): array
    {
        $arr = [];
        foreach ($this->messages as $message) {
            $key = $message->key;
            if ($key) {
                $arr[$key][] = $message->message;
            } else {
                $arr[] = $message->message;
            }
        }

        return $arr;
    }

    protected function getKey($key, $keyParent): ?string
    {
        if (is_string($key) || is_string($keyParent)) {
            $parts = array_filter([$keyParent, $key]);

            return implode(':', $parts);
        }

        return null;
    }
}
