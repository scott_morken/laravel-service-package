<?php

namespace Smorken\Service\Services\VO;

class VOResult implements \Smorken\Service\Contracts\Services\VO\VOResult
{
    protected array $attributes = [];

    public function __get(string $key): mixed
    {
        return $this->attributes[$key] ?? null;
    }

    public function __set(string $key, mixed $value): void
    {
        $this->attributes[$key] = $value;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes): void
    {
        foreach ($attributes as $k => $v) {
            $this->$k = $v;
        }
    }

    public function newInstance(array $attributes = []): static
    {
        // @phpstan-ignore new.static
        return new static(...$attributes);
    }
}
