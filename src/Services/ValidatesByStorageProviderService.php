<?php

namespace Smorken\Service\Services;

use Smorken\Service\Contracts\Services\HasValidatorService;
use Smorken\Service\Services\Traits\HasValidatorServiceTrait;
use Smorken\Service\Services\Traits\ValidateByStorageProviderTrait;
use Smorken\Storage\Contracts\Base as StorageProvider;

class ValidatesByStorageProviderService extends StorageProviderService implements HasValidatorService
{
    use HasValidatorServiceTrait, ValidateByStorageProviderTrait;

    public function __construct(
        protected StorageProvider $provider,
        array $services = []
    ) {
        parent::__construct($this->provider, $services);
    }
}
