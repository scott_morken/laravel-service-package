<?php

namespace Smorken\Service\Services;

use Smorken\Storage\Contracts\Base;

class ValidationRulesByStorageProviderService extends ValidationRulesService
{
    public function __construct(protected Base $provider, $services = [])
    {
        parent::__construct($services);
        $this->setBaseRules($this->provider->validationRules());
    }
}
