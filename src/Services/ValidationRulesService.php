<?php

namespace Smorken\Service\Services;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Enums\SaveTypes;

class ValidationRulesService extends BaseService implements \Smorken\Service\Contracts\Services\ValidationRulesService
{
    protected array $baseRules = [];

    public function getBaseRules(): array
    {
        return $this->baseRules;
    }

    public function setBaseRules(array $rules): self
    {
        $this->baseRules = $rules;

        return $this;
    }

    /**
     * @param  SaveTypes::*  $saveType
     */
    public function getRules(
        string $saveType,
        ?Request $request = null
    ): array {
        return $this->modifyRules($this->getBaseRules(), $saveType, $request);
    }

    /**
     * @param  SaveTypes::*  $saveType
     */
    protected function modifyRules(
        array $rules,
        string $saveType,
        ?Request $request = null
    ): array {
        if ($saveType === SaveTypes::CREATE) {
            return $this->modifyRulesForCreate($rules, $request);
        }
        if ($saveType === SaveTypes::UPDATE) {
            return $this->modifyRulesForUpdate($rules, $request);
        }

        return $rules;
    }

    protected function modifyRulesForCreate(array $rules, ?Request $request): array
    {
        return $rules;
    }

    protected function modifyRulesForUpdate(array $rules, ?Request $request): array
    {
        return $rules;
    }
}
