<?php

namespace Smorken\Service\Services;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Smorken\Service\Services\Traits\HasRequestServiceTrait;

class ValidatorService extends BaseService implements \Smorken\Service\Contracts\Services\ValidatorService
{
    use HasRequestServiceTrait;

    protected array $data = [];

    protected ?Request $request = null;

    public function __construct(protected Factory $validationFactory, array $services = [])
    {
        parent::__construct($services);
    }

    public function attributes(): array
    {
        return [];
    }

    public function fromData(array $data, array $rules): Validator
    {
        $this->data = $data;

        return $this->getValidator($data, $this->rules($rules), $this->messages(), $this->attributes());
    }

    public function fromRequest(Request $request, array $rules): Validator
    {
        $this->request = $this->getRequestFromRequestService($request);

        return $this->fromData($this->validationDataFromRequest($this->getRequest()), $rules);
    }

    public function getValidationFactory(): ?Factory
    {
        return $this->validationFactory;
    }

    public function getValidator(array $data, array $rules, array $messages = [], array $attributes = []): Validator
    {
        return $this->getValidationFactory()->make($data, $rules, $messages, $attributes);
    }

    public function messages(): array
    {
        return [];
    }

    public function rules(array $rules): array
    {
        return $rules;
    }

    public function setValidationFactory(Factory $validationFactory): void
    {
        $this->validationFactory = $validationFactory;
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate(array $data, array $rules): bool
    {
        $validator = $this->fromData($data, $rules);

        return $this->fromValidator($validator);
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validateRequest(Request $request, array $rules): bool
    {
        $validator = $this->fromRequest($request, $rules);

        return $this->fromValidator($validator);
    }

    public function validationDataFromRequest(Request $request): array
    {
        return $request->all();
    }

    protected function failedValidation(Validator $validator): void
    {
        throw new ValidationException($validator);
    }

    protected function fromValidator(Validator $validator): bool
    {
        if ($validator->fails()) {
            $this->failedValidation($validator);
        }

        return true;
    }

    protected function getRequest(): ?Request
    {
        return $this->request;
    }
}
