<?php
declare(strict_types=1);

namespace Tests\Smorken\Service\Phpstan\Services\VO;

use Smorken\Service\Services\VO\ModelResult;

$r = new ModelResult(null, 0, false);
assert($r->model === null);
