<?php

declare(strict_types=1);

namespace Tests\Smorken\Service\Phpstan\Services\VO;

use Smorken\Service\Services\VO\VOResult;

$vo = new VOResult;
$vo->foo = 'bar';
assert($vo->foo === 'bar');
