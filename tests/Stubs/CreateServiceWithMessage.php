<?php

namespace Tests\Smorken\Service\Stubs;

use Smorken\Model\Contracts\Model;
use Smorken\Service\Services\CreateByStorageProviderService;

class CreateServiceWithMessage extends CreateByStorageProviderService
{
    protected function postCreate(?Model $model, array $attributes): ?Model
    {
        $this->addMessage('flash:info', 'Test message');

        return $model;
    }
}
