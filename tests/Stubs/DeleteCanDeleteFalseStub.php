<?php

namespace Tests\Smorken\Service\Stubs;

use Smorken\Model\Contracts\Model;
use Smorken\Service\Services\DeleteByStorageProviderService;

class DeleteCanDeleteFalseStub extends DeleteByStorageProviderService
{
    public function canDelete(Model $model): bool
    {
        return false;
    }
}
