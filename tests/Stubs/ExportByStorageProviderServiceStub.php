<?php

namespace Tests\Smorken\Service\Stubs;

use Smorken\Export\Contracts\Export;
use Smorken\Service\Services\ExportByStorageProviderService;

class ExportByStorageProviderServiceStub extends ExportByStorageProviderService
{
    protected function getHeaderCallback(...$params): ?callable
    {
        return function (Export $exporter, array $headers, mixed $firstModel) {
            return array_merge($headers, ['blahblah']);
        };
    }

    protected function getRowCallback(...$params): ?callable
    {
        return function (Export $exporter, array $data, mixed $models) {
            return [array_merge($data, ['blahblah' => 'yippee'])];
        };
    }
}
