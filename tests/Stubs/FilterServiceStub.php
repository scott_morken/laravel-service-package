<?php

namespace Tests\Smorken\Service\Stubs;

use Illuminate\Http\Request;
use Smorken\Service\Services\FilterService;
use Smorken\Support\Contracts\Filter;

class FilterServiceStub extends FilterService
{
    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'f_test' => $request->input('test'),
            'f_foo' => $request->input('foo'),
        ]);
    }
}
