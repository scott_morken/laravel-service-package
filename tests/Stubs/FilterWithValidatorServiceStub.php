<?php

namespace Tests\Smorken\Service\Stubs;

use Illuminate\Http\Request;
use Smorken\Service\Services\FilterService;
use Smorken\Support\Contracts\Filter;

class FilterWithValidatorServiceStub extends FilterService
{
    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter(['foo' => $request->input('foo')]);
    }

    protected function getRules(): array
    {
        return ['foo' => 'required'];
    }
}
