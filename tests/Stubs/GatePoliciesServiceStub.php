<?php

namespace Tests\Smorken\Service\Stubs;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Service\Services\GatePoliciesService;

class GatePoliciesServiceStub extends GatePoliciesService
{
    protected string $modelClass = TestModel::class;

    public function before(Authenticatable $user, $ability): ?bool
    {
        if ($user->getAuthIdentifier() === 99) {
            return true;
        }

        return null;
    }

    protected function isUserAllowedToView(Authenticatable $user, mixed $model): Response
    {
        if ($user->getAuthIdentifier() === $model->owner_id) {
            return $this->allow();
        }

        return parent::isUserAllowedToView($user, $model);
    }
}
