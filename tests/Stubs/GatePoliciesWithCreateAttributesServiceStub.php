<?php

namespace Tests\Smorken\Service\Stubs;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Service\Services\GatePoliciesService;

class GatePoliciesWithCreateAttributesServiceStub extends GatePoliciesService
{
    protected string $modelClass = TestModel::class;

    protected function isUserAllowedToCreate(Authenticatable $user, ?array $attributes = null): Response
    {
        return ($attributes['foo'] ?? null) === 'bar' ? $this->allow() : $this->deny();
    }
}
