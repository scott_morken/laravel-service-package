<?php

namespace Tests\Smorken\Service\Stubs;

use Smorken\Service\Invokables\Invokable;

class InvokableServiceStub extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind('foo', function ($app) {
            return 'bar';
        });
    }
}
