<?php

namespace Tests\Smorken\Service\Stubs;

use Smorken\Service\Services\VO\ModelResult;

class ModelResultStub extends ModelResult {}
