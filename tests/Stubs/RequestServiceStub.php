<?php

namespace Tests\Smorken\Service\Stubs;

use Illuminate\Http\Request;
use Smorken\Service\Services\RequestService;

class RequestServiceStub extends RequestService
{
    protected function modifyRequest(Request $request): Request
    {
        return $request->merge(['foo' => 'fiz']);
    }
}
