<?php

namespace Tests\Smorken\Service\Stubs;

use Smorken\Service\Services\SanitizeService;

class SanitizeServiceStub extends SanitizeService
{
    protected function handleArraySanitizing(array $array): array
    {
        $array['foo'] = $this->getSanitizer()->string($array['foo'] ?? null);
        $array['bar'] = $this->getSanitizer()->int($array['bar'] ?? null);

        return $array;
    }
}
