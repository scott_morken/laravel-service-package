<?php

namespace Tests\Smorken\Service\Stubs;

use Smorken\Model\Contracts\Model;
use Smorken\Service\Services\UpdateByStorageProviderService;

class UpdateServiceWithMessage extends UpdateByStorageProviderService
{
    protected function postUpdate(Model $model, array $attributes): Model
    {
        $this->addMessage('flash:info', 'Test message');

        return parent::postUpdate($model, $attributes);
    }
}
