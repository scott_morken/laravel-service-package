<?php

namespace Tests\Smorken\Service\Unit\Invokables;

use Illuminate\Contracts\Foundation\Application;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Invokables\InvokableServiceException;
use Smorken\Service\Invokables\Invoker;
use Tests\Smorken\Service\Stubs\InvokableServiceStub;

class InvokerTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testAutoloadWithoutConfigPathIsException(): void
    {
        $sut = new Invoker(['autoload' => true], '/app');
        $this->expectException(InvokableServiceException::class);
        $this->expectExceptionMessage('Path is not set.');
        $sut->handle(m::mock(Application::class));
    }

    public function testAutoloadWithoutValidPathIsException(): void
    {
        $sut = new Invoker(['autoload' => true, 'path' => 'tests/Stubs'], '/foo/bar');
        $this->expectException(InvokableServiceException::class);
        $this->expectExceptionMessage('Path does not exist.');
        $sut->handle(m::mock(Application::class));
    }

    public function testHandleArray(): void
    {
        $sut = new Invoker([
            'autoload' => false,
            'path' => 'tests/Stubs',
            'invokables' => [
                InvokableServiceStub::class,
            ],
        ], '/app');
        $app = m::mock(Application::class);
        $app->shouldReceive('bind')
            ->once()
            ->with('foo', m::type('callable'))
            ->andReturnUsing(function (string $name, callable $callback) use ($app) {
                $this->assertEquals('bar', $callback($app));
            });
        $sut->handle($app);
    }

    public function testHandleAutoload(): void
    {
        $sut = new Invoker(['autoload' => true, 'path' => 'tests/Stubs'], '/app');
        $app = m::mock(Application::class);
        $app->shouldReceive('bind')
            ->once()
            ->with('foo', m::type('callable'))
            ->andReturnUsing(function (string $name, callable $callback) use ($app) {
                $this->assertEquals('bar', $callback($app));
            });
        $sut->handle($app);
    }

    public function testHandleBoth(): void
    {
        $sut = new Invoker([
            'autoload' => true,
            'path' => 'tests/Stubs',
            'invokables' => [
                InvokableServiceStub::class,
            ],
        ], '/app');
        $app = m::mock(Application::class);
        $app->shouldReceive('bind')
            ->twice()
            ->with('foo', m::type('callable'))
            ->andReturnUsing(function (string $name, callable $callback) use ($app) {
                $this->assertEquals('bar', $callback($app));
            });
        $sut->handle($app);
    }
}
