<?php

namespace Tests\Smorken\Service\Unit\Invokables\Invokers;

use Illuminate\Contracts\Foundation\Application;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Invokables\Invokers\FromArray;
use Smorken\Service\ServiceProvider;
use Tests\Smorken\Service\Stubs\InvokableServiceStub;

class FromArrayTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCanInvokeInvokable(): void
    {
        $sut = new FromArray([
            InvokableServiceStub::class,
        ]);
        $app = m::mock(Application::class);
        $app->shouldReceive('bind')
            ->once()
            ->with('foo', m::type('callable'))
            ->andReturnUsing(function (string $name, callable $callback) use ($app) {
                $this->assertEquals('bar', $callback($app));
            });
        $handled = $sut->handle($app);
        $this->assertTrue(in_array(InvokableServiceStub::class, $handled));
    }

    public function testNotInstanceOfInvokableSkips(): void
    {
        $sut = new FromArray([
            ServiceProvider::class,
        ]);
        $handled = $sut->handle(m::mock(Application::class));
        $this->assertEmpty($handled);
    }
}
