<?php

namespace Tests\Smorken\Service\Unit\Invokables\Invokers;

use Illuminate\Contracts\Foundation\Application;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Invokables\InvokableServiceException;
use Smorken\Service\Invokables\Invokers\FromFilesystem;
use Tests\Smorken\Service\Stubs\InvokableServiceStub;

class FromFilesystemTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCanIterateInvokables(): void
    {
        $sut = new FromFilesystem('/app/tests/Stubs');
        $app = m::mock(Application::class);
        $app->shouldReceive('bind')
            ->once()
            ->with('foo', m::type('callable'))
            ->andReturnUsing(function (string $name, callable $callback) use ($app) {
                $this->assertEquals('bar', $callback($app));
            });
        $handled = $sut->handle($app);
        $this->assertCount(1, $handled);
        $this->assertTrue(in_array(InvokableServiceStub::class, $handled));
    }

    public function testMissingPathIsException(): void
    {
        $this->expectException(InvokableServiceException::class);
        $this->expectExceptionMessage('/foo/bar does not exist.');
        $sut = new FromFilesystem('/foo/bar');
    }
}
