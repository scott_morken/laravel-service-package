<?php

namespace Tests\Smorken\Service\Unit\Invokables;

use PHPUnit\Framework\TestCase;
use Smorken\Service\Invokables\TokenParser;
use Smorken\Service\ServiceProvider;

class TokenParserTest extends TestCase
{
    public function testCanGetClassFromFile(): void
    {
        $this->assertEquals(ServiceProvider::class, TokenParser::getClass('/app/src/ServiceProvider.php'));
    }

    public function testCanGetClassNullFromMissingFile(): void
    {
        $this->assertNull(TokenParser::getClass('/foo/bar.php'));
    }
}
