<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Services\AllByStorageProviderService;
use Smorken\Storage\Contracts\Base;

class AllByStorageProviderServiceTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCanGetCollection(): void
    {
        $sut = new AllByStorageProviderService(m::mock(Base::class));
        $collection = new Collection;
        $sut->getProvider()->shouldReceive('all')
            ->once()
            ->andReturn($collection);
        $result = $sut->getByRequest(new Request);
        $this->assertSame($collection, $result->models);
    }
}
