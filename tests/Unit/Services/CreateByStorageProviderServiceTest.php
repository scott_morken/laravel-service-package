<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Model\VO;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\RequestService;
use Smorken\Service\Contracts\Services\StorageProviderService;
use Smorken\Service\Contracts\Services\ValidationRulesService;
use Smorken\Service\Services\CreateByStorageProviderService;
use Smorken\Service\Services\GateService;
use Smorken\Service\Services\ValidatorService;
use Smorken\Storage\Contracts\Base;
use stdClass;
use Tests\Smorken\Service\Stubs\CreateServiceWithMessage;
use Tests\Smorken\Service\Stubs\RequestServiceStub;

class CreateByStorageProviderServiceTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCreateFromRequest(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new CreateByStorageProviderService(m::mock(Base::class));
        $model = new VO(['foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('create')
            ->once()
            ->with(['foo' => 'bar'])
            ->andReturn($model);
        $result = $sut->createFromRequest($request);
        $this->assertSame($model, $result->model);
    }

    public function testCreateFromRequestFailsValidation(): void
    {
        $request = new Request(['bar' => 'biz']);
        $sut = new CreateByStorageProviderService(m::mock(Base::class), [
            \Smorken\Service\Contracts\Services\ValidatorService::class => new ValidatorService(new Factory(new Translator(new ArrayLoader,
                'en'))),
        ]);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('validationRules')->andReturn(['foo' => 'required']);
        $sut->getProvider()->shouldReceive('create')
            ->never();
        $this->expectException(ValidationException::class);
        $result = $sut->createFromRequest($request);
    }

    public function testCreateFromRequestPassesValidation(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new CreateByStorageProviderService(m::mock(Base::class), [
            \Smorken\Service\Contracts\Services\ValidatorService::class => new ValidatorService(new Factory(new Translator(new ArrayLoader,
                'en'))),
        ]);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('validationRules')->andReturn(['foo' => 'required']);
        $model = new VO(['foo' => 'bar']);
        $sut->getProvider()->shouldReceive('create')
            ->once()
            ->with(['foo' => 'bar'])
            ->andReturn($model);
        $result = $sut->createFromRequest($request);
        $this->assertSame($model, $result->model);
    }

    public function testCreateFromRequestPassesValidationWithValidationRulesService(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new CreateByStorageProviderService(m::mock(Base::class), [
            \Smorken\Service\Contracts\Services\ValidatorService::class => new ValidatorService(new Factory(new Translator(new ArrayLoader,
                'en'))),
            ValidationRulesService::class => (new \Smorken\Service\Services\ValidationRulesService)->setBaseRules(['foo' => 'required']),
        ]);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('validationRules')->never();
        $model = new VO(['foo' => 'bar']);
        $sut->getProvider()->shouldReceive('create')
            ->once()
            ->with(['foo' => 'bar'])
            ->andReturn($model);
        $result = $sut->createFromRequest($request);
        $this->assertSame($model, $result->model);
    }

    public function testCreateFromRequestService(): void
    {
        $request = new Request(['foo' => 'bar', 'fiz' => 'buz']);
        $requestService = new RequestServiceStub;
        $sut = new CreateByStorageProviderService(m::mock(Base::class), [RequestService::class => $requestService]);
        $model = new VO(['foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('create')
            ->once()
            ->with(['foo' => 'fiz', 'fiz' => 'buz'])
            ->andReturn($model);
        $result = $sut->createFromRequest($request);
        $this->assertSame($model, $result->model);
    }

    public function testCreateFromRequestWithGateCanAllow(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $gate->define('VO.create', function ($user) {
            return true;
        });
        $request = new Request(['foo' => 'bar']);
        $sut = new CreateByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $model = new VO(['foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('create')
            ->once()
            ->with(['foo' => 'bar'])
            ->andReturn($model);
        $result = $sut->createFromRequest($request);
        $this->assertSame($model, $result->model);
    }

    public function testCreateFromRequestWithGateCanThrowException(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $gate->define('VO.create', function ($user) {
            return false;
        });
        $request = new Request(['foo' => 'bar']);
        $sut = new CreateByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $model = new VO(['foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('create')
            ->never();
        $this->expectException(AuthorizationException::class);
        $result = $sut->createFromRequest($request);
    }

    public function testCreateFromRequestWithGateUsesAttributesCanAllow(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $gate->define('VO.create', function ($user, $attributes) {
            return ($attributes['foo'] ?? null) === 'bar' && $user->id === 1234;
        });
        $request = new Request(['foo' => 'bar']);
        $sut = new CreateByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $model = new VO(['foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('create')
            ->once()
            ->with(['foo' => 'bar'])
            ->andReturn($model);
        $result = $sut->createFromRequest($request);
        $this->assertSame($model, $result->model);
    }

    public function testCreateFromRequestWithMessage(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new CreateServiceWithMessage(m::mock(Base::class));
        $model = new VO(['foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('create')
            ->once()
            ->with(['foo' => 'bar'])
            ->andReturn($model);
        $result = $sut->createFromRequest($request);
        $this->assertSame($model, $result->model);
        $this->assertEquals(['flash:info' => ['Test message']], $result->messagesToArray());
    }

    public function testCreateNotAuthorizedIsException(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('VO.create', function (?stdClass $user) {
            return false;
        });
        $gs = new GateService($gate);
        $request = new Request(['foo' => 'bar']);
        $sut = new CreateByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => $gs]);
        $model = new VO(['foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('create')
            ->never();
        $this->expectException(AuthorizationException::class);
        $result = $sut->createFromRequest($request);
    }

    public function testGetAttributesFromRequest(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new CreateByStorageProviderService(m::mock(Base::class));
        $expected = ['foo' => 'bar'];
        $this->assertEquals($expected, $sut->getAttributesFromRequest($request));
    }

    public function testImplementsInterface(): void
    {
        $sut = new CreateByStorageProviderService(m::mock(Base::class));
        $this->assertTrue($sut->implements(BaseService::class));
        $this->assertTrue($sut->implements(StorageProviderService::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\CreateServiceByStorageProviderService::class));
    }
}
