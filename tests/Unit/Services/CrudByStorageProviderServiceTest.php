<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Contracts\Services\RetrieveService;
use Smorken\Service\Services\CreateByStorageProviderService;
use Smorken\Service\Services\CrudByStorageProviderServices;
use Smorken\Service\Services\DeleteByStorageProviderService;
use Smorken\Service\Services\GateService;
use Smorken\Service\Services\RetrieveByStorageProviderService;
use Smorken\Service\Services\UpdateByStorageProviderService;
use Smorken\Storage\Contracts\Base;

class CrudByStorageProviderServiceTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCanSetGateServiceWhenNotSet(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $services = [
            RetrieveService::class => new RetrieveByStorageProviderService(m::mock(Base::class)),
        ];
        $sut = new CrudByStorageProviderServices($services,
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $this->assertSame($gate, $sut->getRetrieveService()->getGateService()->getGate());
    }

    public function testCreateByStorageProvider(): void
    {
        $provider = m::mock(Base::class);
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $sut = CrudByStorageProviderServices::createByStorageProvider($provider,
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        foreach ($sut->getServices() as $interface => $impl) {
            $this->assertSame($provider, $impl->getProvider());
        }
    }

    public function testSetServiceThrowsExceptionWhenNotServiceInterface(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $services = [
            \Smorken\Service\Contracts\Services\StorageProviderService::class => new RetrieveByStorageProviderService(m::mock(Base::class)),
        ];
        $this->expectException(\OutOfBoundsException::class);
        $sut = new CrudByStorageProviderServices($services,
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
    }

    public function testSettersAndGetters(): void
    {
        $sut = new CrudByStorageProviderServices([]);
        $cs = new CreateByStorageProviderService(m::mock(Base::class));
        $sut->setCreateService($cs);
        $this->assertSame($cs, $sut->getCreateService());
        $ds = new DeleteByStorageProviderService(m::mock(Base::class));
        $sut->setDeleteService($ds);
        $this->assertSame($ds, $sut->getDeleteService());
        $rs = new RetrieveByStorageProviderService(m::mock(Base::class));
        $sut->setRetrieveService($rs);
        $this->assertSame($rs, $sut->getRetrieveService());
        $us = new UpdateByStorageProviderService(m::mock(Base::class));
        $sut->setUpdateService($us);
        $this->assertSame($us, $sut->getUpdateService());
    }
}
