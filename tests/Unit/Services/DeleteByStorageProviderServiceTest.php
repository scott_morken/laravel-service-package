<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Model\Contracts\Model;
use Smorken\Model\VO;
use Smorken\Service\Services\DeleteByStorageProviderService;
use Smorken\Service\Services\GateService;
use Smorken\Storage\Contracts\Base;
use Tests\Smorken\Service\Stubs\DeleteCanDeleteFalseStub;
use Tests\Smorken\Service\Stubs\ModelResultStub;

class DeleteByStorageProviderServiceTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testDeleteFromRequest(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new DeleteByStorageProviderService(m::mock(Base::class));
        $model = new VO(['id' => 1, 'foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getProvider()->shouldReceive('delete')
            ->once()
            ->with($model)
            ->andReturn(true);
        $result = $sut->deleteFromRequest($request, 1);
        $this->assertSame($model, $result->model);
        $this->assertTrue($result->result);
        $this->assertEmpty($result->messages);
    }

    public function testDeleteFromRequestCanDeleteIsFalse(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new DeleteCanDeleteFalseStub(m::mock(Base::class));
        $model = new VO(['id' => 1, 'foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getProvider()->shouldReceive('delete')
            ->never();
        $result = $sut->deleteFromRequest($request, 1);
        $this->assertSame($model, $result->model);
        $this->assertFalse($result->result);
        $expected = ['flash:danger' => ['Delete not allowed for [1].']];
        $this->assertEquals($expected, $result->messagesToArray());
    }

    public function testDeleteFromRequestWithGateCanAllow(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $gate->define('VO.delete', function ($user, ?Model $model) {
            return $model->id === 1;
        });
        $request = new Request(['foo' => 'bar']);
        $sut = new DeleteByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $model = new VO(['id' => 1, 'foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getProvider()->shouldReceive('delete')
            ->once()
            ->with($model)
            ->andReturn(true);
        $result = $sut->deleteFromRequest($request, 1);
        $this->assertSame($model, $result->model);
        $this->assertTrue($result->result);
        $this->assertEmpty($result->messages);
    }

    public function testDeleteFromRequestWithGateCanThrowException(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $gate->define('VO.delete', function ($user, ?Model $model) {
            return $model->id === 1;
        });
        $request = new Request(['foo' => 'bar']);
        $sut = new DeleteByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $model = new VO(['id' => 2, 'foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getProvider()->shouldReceive('delete')
            ->never();
        $this->expectException(AuthorizationException::class);
        $result = $sut->deleteFromRequest($request, 1);
    }

    public function testImplementsInterface(): void
    {
        $sut = new DeleteByStorageProviderService(m::mock(Base::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\BaseService::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\StorageProviderService::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\DeleteServiceByStorageProviderService::class));
    }

    public function testSetVoClass(): void
    {
        $sut = new DeleteByStorageProviderService(m::mock(Base::class));
        $sut->setVOClass(ModelResultStub::class);
        $model = new VO(['id' => 1, 'foo' => 'bar']);
        $m = $sut->newVO(['model' => $model, 'id' => $model->id, 'result' => false, 'messages' => []]);
        $this->assertInstanceOf(ModelResultStub::class, $m);
    }
}
