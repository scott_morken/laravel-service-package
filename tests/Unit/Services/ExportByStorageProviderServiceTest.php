<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use League\Csv\Writer;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Export\Contracts\Export;
use Smorken\Model\VO;
use Smorken\Service\Contracts\Services\ExportByStorageProviderService;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Storage\Contracts\Base;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Service\Stubs\ExportByStorageProviderServiceStub;
use Tests\Smorken\Service\Stubs\FilterServiceStub;

class ExportByStorageProviderServiceTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testExportWithCallbacks(): void
    {
        $sut = new ExportByStorageProviderServiceStub($this->getCsvExporter(), m::mock(Base::class));
        $sut->getProvider()->shouldReceive('chunkFilter')
            ->once()
            ->with(m::type(Filter::class), m::type('callable'))
            ->andReturnUsing(function (Filter $filter, callable $closure) {
                $this->assertEmpty($filter->toArray());
                $models = new Collection([
                    new VO(['id' => 1, 'foo' => 'foo 1']),
                    new VO(['id' => 2, 'foo' => 'foo 2']),
                    new VO(['id' => 3, 'foo' => 'foo 3']),
                ]);
                $closure($models);
            });
        $result = $sut->export(new Request);
        $this->compareResults("id,foo,blahblah\n1,\"foo 1\",yippee\n2,\"foo 2\",yippee\n3,\"foo 3\",yippee\n", $this->runExport($result->exporter));
    }

    public function testExportWithFilterService(): void
    {
        $sut = $this->getSut([
            FilterService::class => new FilterServiceStub,
        ]);
        $sut->getProvider()->shouldReceive('chunkFilter')
            ->once()
            ->with(m::type(Filter::class), m::type('callable'))
            ->andReturnUsing(function (Filter $filter, callable $closure) {
                $this->assertEquals([
                    'f_test' => null,
                    'f_foo' => 'fizbuz',
                ], $filter->toArray());
                $models = new Collection([
                    new VO(['id' => 1, 'foo' => 'foo 1']),
                    new VO(['id' => 2, 'foo' => 'foo 2']),
                    new VO(['id' => 3, 'foo' => 'foo 3']),
                ]);
                $closure($models);
            });
        $result = $sut->export(new Request(['foo' => 'fizbuz']));
        $this->compareResults("id,foo\n1,\"foo 1\"\n2,\"foo 2\"\n3,\"foo 3\"\n", $this->runExport($result->exporter));
    }

    public function testExportWithoutFilterService(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('chunkFilter')
            ->once()
            ->with(m::type(Filter::class), m::type('callable'))
            ->andReturnUsing(function (Filter $filter, callable $closure) {
                $this->assertEmpty($filter->toArray());
                $models = new Collection([
                    new VO(['id' => 1, 'foo' => 'foo 1']),
                    new VO(['id' => 2, 'foo' => 'foo 2']),
                    new VO(['id' => 3, 'foo' => 'foo 3']),
                ]);
                $closure($models);
            });
        $result = $sut->export(new Request);
        $this->compareResults("id,foo\n1,\"foo 1\"\n2,\"foo 2\"\n3,\"foo 3\"\n", $this->runExport($result->exporter));
    }

    protected function compareResults($expected_string, $result_string): void
    {
        $results = explode(PHP_EOL, $result_string);
        $expected = explode(PHP_EOL, $expected_string);
        foreach ($expected as $i => $line) {
            $this->assertEquals($line, $results[$i]);
        }
    }

    protected function getCsvExporter(): Export
    {
        $backend = Writer::createFromFileObject(new \SplTempFileObject);

        return new \Smorken\Export\Handlers\Csv($backend);
    }

    protected function getSut(array $services = []): ExportByStorageProviderService
    {
        return new \Smorken\Service\Services\ExportByStorageProviderService($this->getCsvExporter(),
            m::mock(Base::class), $services);
    }

    protected function runExport(Export $sut): string
    {
        ob_start();
        call_user_func_array([$sut, 'output'], ['test']);

        return ob_get_clean();
    }
}
