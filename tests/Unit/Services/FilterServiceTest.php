<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Http\Request;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Contracts\Services\RequestService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Services\FilterService;
use Tests\Smorken\Service\Stubs\FilterServiceStub;
use Tests\Smorken\Service\Stubs\FilterWithValidatorServiceStub;
use Tests\Smorken\Service\Stubs\RequestServiceStub;

class FilterServiceTest extends TestCase
{
    public function testCreateFilterFromRequestDefaultIsEmpty(): void
    {
        $sut = new FilterService;
        $this->assertEmpty($sut->getFilterFromRequest(new Request(['foo' => 'bar']))->toArray());
    }

    public function testCreateFilterFromRequestService(): void
    {
        $sut = new FilterServiceStub([RequestService::class => new RequestServiceStub]);
        $filter = $sut->getFilterFromRequest(new Request(['foo' => 'bar']));
        $this->assertEquals('fiz', $filter->getAttribute('f_foo'));
    }

    public function testCreateFilterWithValidatorService(): void
    {
        $sut = new FilterWithValidatorServiceStub([
            ValidatorService::class => new \Smorken\Service\Services\ValidatorService(new Factory(new Translator(new ArrayLoader,
                'en'))),
        ]);
        $request = new Request;
        $this->expectException(ValidationException::class);
        $sut->getFilterFromRequest($request);
    }
}
