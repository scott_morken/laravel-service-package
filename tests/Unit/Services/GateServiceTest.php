<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Services\GateService;
use stdClass;
use Tests\Smorken\Service\Stubs\User;

class GateServiceTest extends TestCase
{
    public function testCheckCanBeException(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('test', function (?stdClass $user) {
            return false;
        });
        $sut = new GateService($gate);
        $this->expectException(AuthorizationException::class);
        $sut->check('test');
    }

    public function testCheckCanBeFalse(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('test', function (?stdClass $user) {
            return false;
        });
        $sut = new GateService($gate);
        $this->assertFalse($sut->check('test', null, false));
    }

    public function testCheckCanBeFalseWithArrayContext(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('test', function (?stdClass $user, bool $continue) {
            return $continue;
        });
        $sut = new GateService($gate);
        $this->assertFalse($sut->check('test', ['continue' => false], false));
    }

    public function testCheckCanBeFalseWithSingleContext(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('test', function (?stdClass $user, bool $continue) {
            return $continue;
        });
        $sut = new GateService($gate);
        $this->assertFalse($sut->check('test', false, false));
    }

    public function testCheckCanBeFalseWithUser(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 2];
        });
        $gate->define('test', function (?stdClass $user) {
            return $user->id === 1;
        });
        $sut = new GateService($gate);
        $this->assertFalse($sut->check('test', null, false));
    }

    public function testCheckCanBeTrue(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('test', function (?stdClass $user) {
            return true;
        });
        $sut = new GateService($gate);
        $this->assertTrue($sut->check('test', null, false));
    }

    public function testCheckCanBeTrueWithUser(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1];
        });
        $gate->define('test', function (?stdClass $user) {
            return $user->id === 1;
        });
        $sut = new GateService($gate);
        $this->assertTrue($sut->check('test', null, false));
    }

    public function testCheckRequestCanBeException(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('test', function (Authenticatable $user) {
            return $user->getAuthIdentifier() === 1;
        });
        $request = new Request;
        $request->setUserResolver(function () {
            return new User(2);
        });
        $sut = new GateService($gate);
        $this->expectException(AuthorizationException::class);
        $sut->checkRequest($request, 'test');
    }

    public function testCheckRequestCanBeFalseWithoutException(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('test', function (Authenticatable $user) {
            return $user->getAuthIdentifier() === 1;
        });
        $request = new Request;
        $request->setUserResolver(function () {
            return new User(2);
        });
        $sut = new GateService($gate);
        $this->assertFalse($sut->checkRequest($request, 'test', null, false));
    }

    public function testCheckRequestCanBeTrue(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('test', function (Authenticatable $user) {
            return $user->getAuthIdentifier() === 1;
        });
        $request = new Request;
        $request->setUserResolver(function () {
            return new User;
        });
        $sut = new GateService($gate);
        $this->assertTrue($sut->checkRequest($request, 'test'));
    }

    public function testCheckRequestWithContextCanBeTrue(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('test', function (Authenticatable $user, stdClass $continueObject) {
            return $continueObject->continue;
        });
        $request = new Request;
        $request->setUserResolver(function () {
            return new User;
        });
        $sut = new GateService($gate);
        $this->assertTrue($sut->checkRequest($request, 'test', [(object) ['continue' => true]]));
    }

    public function testCheckUndefinedGateIsFalse(): void
    {
        $gate = new Gate(new Container, function () {});
        $sut = new GateService($gate);
        $this->assertFalse($sut->check('foo', null, false));
    }

    public function testImplementsInterface(): void
    {
        $gate = new Gate(new Container, function () {});
        $sut = new GateService($gate);
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\GateService::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\BaseService::class));
    }
}
