<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Contracts\Services\GatePoliciesService;
use Smorken\Service\Services\GateWithGatePoliciesService;
use Tests\Smorken\Service\Stubs\GatePoliciesServiceStub;
use Tests\Smorken\Service\Stubs\GatePoliciesWithCreateAttributesServiceStub;
use Tests\Smorken\Service\Stubs\TestModel;
use Tests\Smorken\Service\Stubs\User;

class GateWithGatePoliciesServiceTest extends TestCase
{
    public function testAuthorizedToCreateWithAttributeValue(): void
    {
        $gate = new Gate(new Container, function () {});
        $request = new Request;
        $request->setUserResolver(function () {
            return new User(99);
        });
        $sut = new GateWithGatePoliciesService($gate, [
            GatePoliciesService::class => new GatePoliciesWithCreateAttributesServiceStub,
        ]);
        $this->assertTrue($sut->checkRequest($request, 'TestModel.create', [['foo' => 'bar']]));
    }

    public function testAuthorizedToCreateWithBeforeOverride(): void
    {
        $gate = new Gate(new Container, function () {});
        $request = new Request;
        $request->setUserResolver(function () {
            return new User(99);
        });
        $sut = new GateWithGatePoliciesService($gate, [
            GatePoliciesService::class => new GatePoliciesServiceStub,
        ]);
        $this->assertTrue($sut->checkRequest($request, 'TestModel.create'));
    }

    public function testAuthorizedToViewWhenMatchingUser(): void
    {
        $gate = new Gate(new Container, function () {});
        $request = new Request;
        $request->setUserResolver(function () {
            return new User(1);
        });
        $sut = new GateWithGatePoliciesService($gate, [
            GatePoliciesService::class => new GatePoliciesServiceStub,
        ]);
        $model = new TestModel(['owner_id' => 1]);
        $this->assertTrue($sut->checkRequest($request, 'TestModel.view', $model));
    }

    public function testNotAuthorizedToCreate(): void
    {
        $gate = new Gate(new Container, function () {});
        $request = new Request;
        $request->setUserResolver(function () {
            return new User(1);
        });
        $sut = new GateWithGatePoliciesService($gate, [
            GatePoliciesService::class => new GatePoliciesServiceStub,
        ]);
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('You do not have permission to create a new record.');
        $sut->checkRequest($request, 'TestModel.create');
    }

    public function testNotAuthorizedToCreateWithoutAttributeValue(): void
    {
        $gate = new Gate(new Container, function () {});
        $request = new Request;
        $request->setUserResolver(function () {
            return new User(99);
        });
        $sut = new GateWithGatePoliciesService($gate, [
            GatePoliciesService::class => new GatePoliciesWithCreateAttributesServiceStub,
        ]);
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('This action is unauthorized.');
        $sut->checkRequest($request, 'TestModel.create', [['foo' => 'buzz']]);
    }

    public function testNotAuthorizedToViewWhenNotMatchingUser(): void
    {
        $gate = new Gate(new Container, function () {});
        $request = new Request;
        $request->setUserResolver(function () {
            return new User(1);
        });
        $sut = new GateWithGatePoliciesService($gate, [
            GatePoliciesService::class => new GatePoliciesServiceStub,
        ]);
        $model = new TestModel(['owner_id' => 99]);
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('You do not have permission to view this record.');
        $sut->checkRequest($request, 'TestModel.view', $model);
    }
}
