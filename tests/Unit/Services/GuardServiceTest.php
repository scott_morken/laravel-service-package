<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Contracts\Auth\Guard;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Services\GuardService;

class GuardServiceTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCanGetAndSet(): void
    {
        $guard = m::mock(Guard::class);
        $sut = new GuardService($guard);
        $this->assertSame($guard, $sut->getGuard());
        $guard2 = m::mock(Guard::class);
        $sut->setGuard($guard2);
        $this->assertSame($guard2, $sut->getGuard());
    }
}
