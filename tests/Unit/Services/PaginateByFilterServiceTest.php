<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Model\VO;
use Smorken\Service\Services\FilterService;
use Smorken\Service\Services\GateService;
use Smorken\Service\Services\PaginateByFilterService;
use Smorken\Storage\Contracts\Base;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Service\Stubs\FilterServiceStub;
use Tests\Smorken\Service\Stubs\TestModel;

class PaginateByFilterServiceTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testGetByRequest(): void
    {
        $sut = new PaginateByFilterService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\FilterService::class => new FilterServiceStub]);
        $request = new Request(['test' => 'foo']);
        $resultCollection = new Collection;
        $sut->getProvider()->shouldReceive('getByFilter')
            ->once()
            ->with(m::type(Filter::class), m::type('int'))
            ->andReturn($resultCollection);
        $sut->getProvider()->shouldReceive('getModel')
            ->once()
            ->andReturn(new TestModel);
        $result = $sut->getByRequest($request);
        $this->assertEquals('foo', $result->filter->f_test);
        $this->assertSame($resultCollection, $result->models);
    }

    public function testGetByRequestWithGateCanAllow(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $gate->define('VO.viewAny', function ($user) {
            return true;
        });
        $sut = new PaginateByFilterService(m::mock(Base::class),
            [
                \Smorken\Service\Contracts\Services\FilterService::class => new FilterServiceStub,
                \Smorken\Service\Contracts\Services\GateService::class => new GateService($gate),
            ]);
        $request = new Request(['test' => 'foo']);
        $resultCollection = new Collection;
        $sut->getProvider()->shouldReceive('getByFilter')
            ->once()
            ->with(m::type(Filter::class), m::type('int'))
            ->andReturn($resultCollection);
        $model = new VO(['foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn($model);
        $result = $sut->getByRequest($request);
        $this->assertEquals('foo', $result->filter->f_test);
        $this->assertSame($resultCollection, $result->models);
    }

    public function testGetByRequestWithGateCanThrowException(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $gate->define('VO.viewAny', function ($user) {
            return false;
        });
        $sut = new PaginateByFilterService(m::mock(Base::class),
            [
                \Smorken\Service\Contracts\Services\FilterService::class => new FilterServiceStub,
                \Smorken\Service\Contracts\Services\GateService::class => new GateService($gate),
            ]);
        $request = new Request(['test' => 'foo']);
        $sut->getProvider()->shouldReceive('getByFilter')
            ->never();
        $model = new VO(['foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn($model);
        $this->expectException(AuthorizationException::class);
        $result = $sut->getByRequest($request);
    }

    public function testImplementsInterface(): void
    {
        $sut = new PaginateByFilterService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\FilterService::class => new FilterService]);
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\PaginateByFilterService::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\BaseService::class));
    }
}
