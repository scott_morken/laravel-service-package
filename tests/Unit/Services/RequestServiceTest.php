<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Smorken\Sanitizer\Actors\Standard;
use Smorken\Service\Contracts\Services\SanitizeService;
use Smorken\Service\Services\RequestService;
use Tests\Smorken\Service\Stubs\RequestServiceStub;
use Tests\Smorken\Service\Stubs\SanitizeServiceStub;

class RequestServiceTest extends TestCase
{
    public function testCanSanitizeRequest(): void
    {
        $s = new \Smorken\Sanitizer\Sanitize([
            'default' => 'standard',
            'sanitizers' => [
                'standard' => Standard::class,
            ],
        ]);
        $service = new SanitizeServiceStub($s);
        $request = new Request(['foo' => 'bar', 'bar' => 'foo', 'fiz' => 'buz']);
        $sut = new RequestService($request, [SanitizeService::class => $service]);
        $request = $sut->getRequest();
        $this->assertEquals(['foo' => 'bar', 'bar' => 0, 'fiz' => 'buz'], $request->input());
    }

    public function testGetAndSet(): void
    {
        $request = new Request;
        $sut = new RequestService($request);
        $this->assertSame($request, $sut->getRequest());
        $request2 = new Request;
        $sut->setRequest($request2);
        $this->assertSame($request2, $sut->getRequest());
    }

    public function testModifyRequest(): void
    {
        $request = new Request(['foo' => 'bar', 'fiz' => 'buz']);
        $this->assertEquals('bar', $request->input('foo'));
        $sut = new RequestServiceStub($request);
        $this->assertEquals('fiz', $sut->getRequest()->input('foo'));
        $this->assertEquals('buz', $sut->getRequest()->input('fiz'));
    }
}
