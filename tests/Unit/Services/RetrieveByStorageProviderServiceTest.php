<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Model\Contracts\Model;
use Smorken\Model\VO;
use Smorken\Service\Contracts\Enums\RetrieveTypes;
use Smorken\Service\Services\GateService;
use Smorken\Service\Services\RetrieveByStorageProviderService;
use Smorken\Storage\Contracts\Base;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RetrieveByStorageProviderServiceTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testFindByIdFailsToFindCanReturnNull(): void
    {
        $sut = new RetrieveByStorageProviderService(m::mock(Base::class));
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturnNull();
        $this->assertNull($sut->findById(1)->model);
    }

    public function testFindByIdFailsToFindCanThrowException(): void
    {
        $sut = new RetrieveByStorageProviderService(m::mock(Base::class));
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andThrow(new NotFoundHttpException('Not found.'));
        $this->expectException(NotFoundHttpException::class);
        $sut->findById(1);
    }

    public function testFindByIdFailsToFindWithGateCanAllow(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $gate->define('VO.view', function ($user, ?Model $model) {
            return $model->id === 1;
        });
        $model = new VO(['id' => 1]);
        $sut = new RetrieveByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $this->assertSame($model, $sut->findById(1)->model);
    }

    public function testFindByIdFailsToFindWithGateCanThrowException(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('VO.view', function ($user, ?Model $model) {
            return $model->id === 1;
        });
        $model = new VO(['id' => 2]);
        $sut = new RetrieveByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $this->expectException(AuthorizationException::class);
        $sut->findById(1);
    }

    public function testFindByIdWithDeleteGateCanThrowException(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('VO.view', function ($user, ?Model $model) {
            return true;
        });
        $gate->define('VO.update', function ($user, ?Model $model) {
            return true;
        });
        $gate->define('VO.delete', function ($user, ?Model $model) {
            return false;
        });
        $model = new VO(['id' => 2]);
        $sut = new RetrieveByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $this->expectException(AuthorizationException::class);
        $sut->findById(1, true, RetrieveTypes::UPDATE);
    }

    public function testFindByIdWithModel(): void
    {
        $sut = new RetrieveByStorageProviderService(m::mock(Base::class));
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $model = new VO;
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturns($model);
        $this->assertSame($model, $sut->findById(1)->model);
    }

    public function testFindByIdWithUpdateGateCanThrowException(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('VO.view', function ($user, ?Model $model) {
            return true;
        });
        $gate->define('VO.update', function ($user, ?Model $model) {
            return false;
        });
        $model = new VO(['id' => 2]);
        $sut = new RetrieveByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $this->expectException(AuthorizationException::class);
        $sut->findById(1, true, RetrieveTypes::UPDATE);
    }

    public function testImplementsInterface(): void
    {
        $sut = new RetrieveByStorageProviderService(m::mock(Base::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\BaseService::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\StorageProviderService::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\RetrieveServiceByStorageProviderService::class));
    }
}
