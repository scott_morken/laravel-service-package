<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Smorken\Sanitizer\Actors\Standard;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\Service\Contracts\Services\SanitizeService;
use Tests\Smorken\Service\Stubs\SanitizeServiceStub;

class SanitizeServiceTest extends TestCase
{
    public function testArraySanitizing(): void
    {
        $request = ['foo' => 9999, 'bar' => 'not a number', 'biz' => 999];
        $sut = $this->getSut();
        $request = $sut->sanitizeArray($request);
        $this->assertEquals([
            'foo' => '9999',
            'bar' => 0,
            'biz' => 999,
        ], $request);
    }

    public function testRequestSanitizing(): void
    {
        $request = new Request(['foo' => 'foobar', 'bar' => 'not a number', 'biz' => 999]);
        $sut = $this->getSut();
        $request = $sut->sanitizeRequest($request);
        $this->assertEquals([
            'foo' => 'foobar',
            'bar' => 0,
            'biz' => 999,
        ], $request->input());
    }

    protected function getSanitizer(): Sanitize
    {
        return new \Smorken\Sanitizer\Sanitize([
            'default' => 'standard',
            'sanitizers' => [
                'standard' => Standard::class,
            ],
        ]);
    }

    protected function getSut(): SanitizeService
    {
        return new SanitizeServiceStub($this->getSanitizer());
    }
}
