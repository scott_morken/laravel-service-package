<?php

namespace Tests\Smorken\Service\Unit\Services;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Model\Contracts\Model;
use Smorken\Model\VO;
use Smorken\Service\Contracts\Services\RequestService;
use Smorken\Service\Services\GateService;
use Smorken\Service\Services\UpdateByStorageProviderService;
use Smorken\Storage\Contracts\Base;
use stdClass;
use Tests\Smorken\Service\Stubs\RequestServiceStub;
use Tests\Smorken\Service\Stubs\UpdateServiceWithMessage;

class UpdateByStorageServiceProviderTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testGetAttributesFromRequest(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new UpdateByStorageProviderService(m::mock(Base::class));
        $expected = ['foo' => 'bar'];
        $this->assertEquals($expected, $sut->getAttributesFromRequest($request));
    }

    public function testImplementsInterface(): void
    {
        $sut = new UpdateByStorageProviderService(m::mock(Base::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\BaseService::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\StorageProviderService::class));
        $this->assertTrue($sut->implements(\Smorken\Service\Contracts\Services\UpdateByStorageProviderService::class));
    }

    public function testUpdateFromRequest(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new UpdateByStorageProviderService(m::mock(Base::class));
        $model = new VO(['id' => 1, 'foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getProvider()->shouldReceive('update')
            ->once()
            ->with($model, ['foo' => 'bar'])
            ->andReturn($model);
        $result = $sut->updateFromRequest($request, 1);
        $this->assertSame($model, $result->model);
    }

    public function testUpdateFromRequestService(): void
    {
        $request = new Request(['foo' => 'bar']);
        $requestService = new RequestServiceStub;
        $sut = new UpdateByStorageProviderService(m::mock(Base::class), [RequestService::class => $requestService]);
        $model = new VO(['id' => 1, 'foo' => 'fiz']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getProvider()->shouldReceive('update')
            ->once()
            ->with($model, ['foo' => 'fiz'])
            ->andReturn($model);
        $result = $sut->updateFromRequest($request, 1);
        $this->assertSame($model, $result->model);
    }

    public function testUpdateFromRequestWithGateCanAllow(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $gate->define('VO.update', function ($user, ?Model $model) {
            return $model->id === 1;
        });
        $request = new Request(['foo' => 'bar']);
        $sut = new UpdateByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $model = new VO(['id' => 1, 'foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getProvider()->shouldReceive('update')
            ->once()
            ->with($model, ['foo' => 'bar'])
            ->andReturn($model);
        $result = $sut->updateFromRequest($request, 1);
        $this->assertSame($model, $result->model);
    }

    public function testUpdateFromRequestWithGateCanThrowException(): void
    {
        $gate = new Gate(new Container, function () {
            return (object) ['id' => 1234];
        });
        $gate->define('VO.update', function ($user, ?Model $model) {
            return $model->id === 1;
        });
        $request = new Request(['foo' => 'bar']);
        $sut = new UpdateByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => new GateService($gate)]);
        $model = new VO(['id' => 2, 'foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getProvider()->shouldReceive('update')
            ->never();
        $this->expectException(AuthorizationException::class);
        $result = $sut->updateFromRequest($request, 1);
    }

    public function testUpdateFromRequestWithMessage(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = new UpdateServiceWithMessage(m::mock(Base::class));
        $model = new VO(['id' => 1, 'foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getProvider()->shouldReceive('update')
            ->once()
            ->with($model, ['foo' => 'bar'])
            ->andReturn($model);
        $result = $sut->updateFromRequest($request, 1);
        $this->assertSame($model, $result->model);
        $this->assertEquals(['flash:info' => ['Test message']], $result->messagesToArray());
    }

    public function testUpdateNotAuthorizedIsException(): void
    {
        $gate = new Gate(new Container, function () {});
        $gate->define('VO.update', function (?stdClass $user, VO $model) {
            return $model->id === 1;
        });
        $gs = new GateService($gate);
        $request = new Request(['foo' => 'bar']);
        $sut = new UpdateByStorageProviderService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\GateService::class => $gs]);
        $model = new VO(['id' => 2, 'foo' => 'bar']);
        $sut->getProvider()->shouldReceive('getModel')->andReturn(new VO);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(2)
            ->andReturn($model);
        $sut->getProvider()->shouldReceive('update')
            ->never();
        $this->expectException(AuthorizationException::class);
        $result = $sut->updateFromRequest($request, 2);
    }
}
